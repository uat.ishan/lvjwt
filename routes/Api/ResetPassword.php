<?php

/**
 * All route names are prefixed with 'api.'.
 */
//route for projects prefix
Route::group(['prefix' => 'reset-password', 'namespace' => 'ResetPassword'], function () {
    Route::post('password/email', 'ForgotPasswordController@getResetToken')->name('getResetToken');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('reset');
});

