<?php

/**
 * All route names are prefixed with 'api.'.
 */
//route for projects prefix
Route::group(['prefix' => 'tasks', 'namespace' => 'Task', 'middleware' => 'auth.jwt'], function () {
    Route::get('/{project_id}', 'TaskController@get_task_by_project_id')->name('index');
    Route::post('/get-task-by-tag-name', 'TaskController@get_tasks_by_tag_name')->name('get_task_by_tag_name');
});

//route for project prefix
Route::group(['prefix' => 'task', 'namespace' => 'Task', 'middleware' => 'auth.jwt'], function () {
    Route::get('/{id}', 'TaskController@get_task_by_id')->name('get_task_by_id');
    Route::post('/change/status', 'TaskController@change_task_status')->name('change_task_status');
    

    Route::patch('/{id}', 'TaskController@update')->name('update');
    Route::put('/create', 'TaskController@create')->name('create');
    Route::delete('/{id}', 'TaskController@destroy')->name('delete');
});
