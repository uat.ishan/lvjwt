<?php

/**
 * All route names are prefixed with 'api.'.
 */
Route::group(['namespace' => 'User', 'prefix' => 'auth/',], function () {
    Route::post('sign-in', 'UserController@signIn')->name('signin');
    Route::put('user', 'UserController@register')->name('add');
    Route::get('logout/{token}', 'UserController@logout')->name('add');
});
Route::group(['namespace' => 'User'], function () {
    Route::put('user', 'UserController@register')->name('add');
});
Route::group(['namespace' => 'User', 'middleware' => 'auth.jwt', 'prefix' => 'user/', 'as' => 'user.'], function () {
//    Route::get('edit', 'UserStatusController@getDeactivated')->name('user.deactivated');
    Route::get('get', 'UserController@get_user')->name('get_user');
    // Route::get('update', 'UserController@dummy')->name('dummy');   
    // Route::patch('update', 'UserController@update_user')->name('update_user');   
});

Route::group(['namespace' => 'User', 'prefix' => 'user/', 'as' => 'user.'], function () {
    Route::patch('update', 'UserController@update_user')->name('update_user');
    Route::post('check_password', 'UserController@check_password')->name('check_password');
    Route::post('change_password', 'UserController@change_password')->name('change_password');
    Route::post('password/email', 'UserController@send_reset_link_email')->name('send_reset_link_email');
    Route::post('password/email', 'UserController@getResetToken')->name('getResetToken');
    Route::post('password/reset', 'UserController@reset')->name('reset');
});


Route::group(['namespace' => 'User', 'middleware' => 'auth.jwt', 'prefix' => 'users', 'as' => 'users.'], function () {
    Route::get('', 'UserController@index')->name('get');
});

