<?php

/**
 * All route names are prefixed with 'api.'.
 */
//route for projects prefix
Route::group(['prefix' => 'projects', 'namespace' => 'Project', 'middleware' => 'auth.jwt'], function () {
    Route::get('', 'ProjectController@index')->name('index');
    Route::get('/pagination', 'ProjectController@project_pagination')->name('project_pagination');
});

//route for project prefix
Route::group(['prefix' => 'project', 'namespace' => 'Project', 'middleware' => 'auth.jwt'], function () {
    Route::post('/get', 'ProjectController@get_projects_by_user_id')->name('get_projects_by_user_id');
    Route::post('/assignee/delete', 'ProjectController@delete_assignee')->name('delete_assignee');
    Route::get('/{handle_name}', 'ProjectController@get_project_by_handle_name')->name('get_project_by_handle_name');
    Route::get('/{handle_name}/statuses', 'ProjectController@get_status_by_handle_name')->name('get_status_by_handle_name');
    Route::get('/{project_id}/status/{status}', 'ProjectController@change_project_status')->name('change_project_status');
    Route::get('{id}/tasks', 'ProjectController@get_tasks_by_project_id')->name('get_tasks_by_project_id');
    Route::patch('/{id}', 'ProjectController@update')->name('update');
    Route::put('/create', 'ProjectController@create')->name('create');
    Route::delete('/{id}', 'ProjectController@destroy')->name('delete');
});

