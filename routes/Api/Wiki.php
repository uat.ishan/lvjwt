<?php

/**
 * All route names are prefixed with 'api.'.
 */
Route::group(['prefix' => 'wiki', 'namespace' => 'Wiki', 'middleware' => 'auth.jwt'], function () {
    
    Route::post('/save', 'WikiController@save_wiki')->name('save_wiki');
    Route::get('/{projectId}', 'WikiController@get_wiki_by_projectId')->name('get_wiki_by_projectId');

});

