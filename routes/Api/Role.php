<?php

/**
 * All route names are prefixed with 'api.'.
 */

/* Routes with prefix of roles */
Route::group([ 'prefix' => 'roles/','namespace' => 'Role'], function () {
    Route::get('/', 'RoleController@index')->name('index');
});

/* Route with prefix of role */
Route::group([ 'prefix' => 'role/','namespace' => 'Role'], function () {
    Route::get('/{id}', 'RoleController@show')->name('show');
    Route::patch('/{id}', 'RoleController@update')->name('update');
    Route::put('/create', 'RoleController@create')->name('create');
    Route::delete('/{id}', 'RoleController@destroy')->name('delete');
});

