<?php

//route for board prefix
Route::group(['prefix' => 'board', 'namespace' => 'Board', 'middleware' => 'auth.jwt'], function () {
    Route::delete('/{id}', 'BoardController@delete_board')->name("delete");
});

