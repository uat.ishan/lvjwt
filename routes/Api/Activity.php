<?php

/**
 * All route names are prefixed with 'api.'.
 */
Route::group(['prefix' => 'activity', 'namespace' => 'Activity', 'middleware' => 'auth.jwt'], function () {
    
    Route::post('/task/save', 'ActivityController@save_activity')->name('save_activity');

});

