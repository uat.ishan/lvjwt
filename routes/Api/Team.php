<?php

/**
 * All route names are prefixed with 'api.'.
 */

//route for teams prefix
Route::group([ 'prefix' => 'teams/','namespace' => 'Team'], function () {
    Route::get('/', 'TeamController@index')->name('index');
});

//route for team prefix
Route::group([ 'prefix' => 'team/','namespace' => 'Team'], function () {
    Route::get('/{id}', 'TeamController@show')->name('show');
    Route::patch('/{id}', 'TeamController@update')->name('update');
    Route::put('/create', 'TeamController@create')->name('create');
    Route::delete('/{id}', 'TeamController@destroy')->name('delete');
});

