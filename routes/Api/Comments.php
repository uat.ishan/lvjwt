<?php

/**
 * All route names are prefixed with 'api.'.
 */
//route for comments prefix
Route::group(['prefix' => 'comments', 'namespace' => 'Comments', 'middleware' => 'auth.jwt'], function () {

    Route::get('', 'CommentsController@index')->name('index');
    Route::get('/{sourceId}', 'CommentsController@get_comments_by_source_id')->name('get_comments_by_source_id');
    Route::post('/find', 'CommentsController@get_comments_by_taskid_sourceid')->name('get_comments_by_taskid_sourceid');
});

//route for comment prefix
Route::group(['prefix' => 'comment', 'namespace' => 'Comments', 'middleware' => 'auth.jwt'], function () {
    
    Route::post('/create', 'CommentsController@save_comment')->name('save_comment');
	Route::get('/{id}', 'CommentsController@get_comment_by_id')->name('get_comment_by_id');

});

