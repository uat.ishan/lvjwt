<?php

/**
 * All route names are prefixed with 'api.'.
 */
Route::group(['prefix' => 'credential', 'namespace' => 'Credentials', 'middleware' => 'auth.jwt'], function () {

    Route::post('/save', 'CredentialsController@save_credential')->name('save_credential');
    Route::get('/{projectId}', 'CredentialsController@get_credential_by_projectId')->name('get_credential_by_projectId');
    
});

