<?php

/**
 * All route names are prefixed with 'api.'.
 */
//route for status prefix
Route::group(['prefix' => 'statuses', 'namespace' => 'Status'], function () {
    Route::get('', 'StatusController@index')->name('index');
});

//route for status prefix
Route::group(['prefix' => 'status', 'namespace' => 'Status', 'middleware' => 'auth.jwt'], function () {
    Route::get('get-last-commit', 'StatusController@getLastCommit')->name('last_commit');
    Route::get('/{id}', 'StatusController@show')->name('show');
    Route::patch('/{id}', 'StatusController@update')->name('update');
    Route::put('/create', 'StatusController@create')->name('create');
    Route::delete('/{id}', 'StatusController@destroy')->name('delete');
});

