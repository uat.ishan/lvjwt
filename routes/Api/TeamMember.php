<?php

/**
 * All route names are prefixed with 'api.'.
 */

//route for team member prefix
Route::group([ 'prefix' => 'team-members/','namespace' => 'TeamMember'], function () {
    Route::get('/', 'TeamMemberController@index')->name('index');
});

//route for team member prefix
Route::group([ 'prefix' => 'team-member/','namespace' => 'TeamMember'], function () {
    Route::get('/{id}', 'TeamMemberController@show')->name('show');
    Route::patch('/{id}', 'TeamMemberController@update')->name('update');
    Route::put('/create', 'TeamMemberController@create')->name('create');
    Route::delete('/{id}', 'TeamMemberController@destroy')->name('delete');
});

