<?php

namespace App\Http\Controllers\Api\Credentials;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Repositories\Api\Credentials\CredentialsRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Http\Request;

/**
 * Class CredentialsController.
 */

class CredentialsController extends Controller {

    public function index() {

    }

    /* Function that will save commment in database from request */
    public function save_credential(Request $request) {
        try{
            $rules = array(
                'data' => 'required'
            );

            /* Validate message field with validator */
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $errors = $validator->getMessageBag()->toArray();
                $response['status'] = FALSE;
                $response['errors'] = $errors;
                $response['message'] = "Some error occure while create.";
                return response()->json($response, 201);
            } else {
                
                /* If message is valid then proceed furthur */
                $saveCredential = CredentialsRepository::save_credential($request);
                if (isset($saveCredential) && !empty($saveCredential) && count($saveCredential) > 0) {
                    $response['status'] = TRUE;
                    $response['credential'] = $saveCredential->toArray();
                    $response['message'] = "Credential saved successfully.";
                    return response()->json($response, 201);
                }
                else{
                    $response['status'] = FALSE;
                    $response['message'] = "Error while saving credential, please try again.";
                    return response()->json($response, 201);
                }
            }
        }
        catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    
    public function get_credential_by_projectId($projectId){
        
        try{
            
            /* Call to repository function that will return single comment from table */
            $credential = CredentialsRepository::get_credential_by_projectId($projectId);
            
            if(!empty($credential) && isset($credential) && count($credential) > 0){
                $response['status'] = TRUE;
                $response['credential'] = $credential->toArray();
                $response['message'] = "Credential fetched successfully.";
                return response()->json($response, 201);
            }
            else{
                $response['status'] = FALSE;
                $response['message'] = "No credential found.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }
    
}