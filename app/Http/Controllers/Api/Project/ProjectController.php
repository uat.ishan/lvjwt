<?php

namespace App\Http\Controllers\Api\Project;

use App\Models\Access\Project\Project;
use App\Models\Access\Status\Status;
use App\Models\Access\User\User;
// use App\Events\Api\ActivityEvent;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Repositories\Api\Project\ProjectRepository;
use App\Repositories\Api\Task\TaskRepository;
use App\Repositories\Api\Status\StatusRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Http\Request;

/**
 * Class ProfileController.
 */

class ProjectController extends Controller {

    //Function for get all Projects
    public function index() {
        $response = [
            'status' => FALSE
        ];

        try {
            $getAllProjects = ProjectRepository::get_all_projects();
            if (isset($getAllProjects) && count($getAllProjects) > 0) {
                $response['status'] = TRUE;
                $response['projects'] = $getAllProjects;
                $response['message'] = "Projects successfully fetched.";
                return response()->json($response, 201);
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Project is not Present.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //Function for get single project Detail
    public function get_project_by_handle_name($handle_name) {
        //function check Project Id is present
        // $checkProjectIdPresent = ProjectRepository::check_project_present($id);
        try {
            $checkProjectIdPresent = 1;
            if ($checkProjectIdPresent) {
                $getSingleProject = Project::where('handle', '=', $handle_name)->first();
                $getProjectAssigneeByProjectId = ProjectRepository::get_project_assignee($getSingleProject->id);
                if (isset($getProjectAssigneeByProjectId)) {
                    $projectAssignees = array();
                    foreach ($getProjectAssigneeByProjectId as $key => $value) {
                        array_push($projectAssignees,$getProjectAssigneeByProjectId[$key]->user);
                    }

                    $projectTaskAssignee = array();

                    /* Call to function that will get assignees of task of project */
                    $projectTaskAssignee = ProjectRepository::get_task_assignee($getSingleProject->id);

                /* Call to function that will return users that are not assigned to any project and task */
                    /* To get vacant user we are am passing task assignees to function*/
                    
                    $availaibleUsers = ProjectRepository::get_availaible_users($projectTaskAssignee);
                    

                    $response['status'] = TRUE;
                    $response['project'] = isset($getSingleProject) && count($getSingleProject) > 0 ? $getSingleProject : array();
                    $response['assignees'] = isset($projectAssignees) && count($projectAssignees)> 0 ? $projectAssignees : array();
                    $response['availaibleAssignees'] = isset($availaibleUsers) && count($availaibleUsers)> 0 ? $availaibleUsers : array();
                    $response['taskAssignees'] = isset($projectTaskAssignee) && count($projectTaskAssignee)> 0 ? $projectTaskAssignee : array();
                    $response['message'] = "Project fetched successfully!!.";
                    return response()->json($response, 201);
                }
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Project is not present.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //Function for update single project Detail
    //$request params send by user for update Project
    public function update(Request $request, $id) {
        //function check Project Id is present
        //validation rule for project create
        $rules = array(
        'handle' => 'required|unique:projects,handle,'.$id.',id',
        'name' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->getMessageBag()->toArray();
            $response['status'] = FALSE;
            $response['errors'] = $errors;
            $response['message'] = "Some error occure while create .";
            return response()->json($response, 201);
        } else {
            $checkProjectIdPresent = ProjectRepository::check_project_present($id);
            if ($checkProjectIdPresent) {
                //call function for update single Project
                $updateSingleProject = ProjectRepository::update_single_project($request, $id);

                if (isset($updateSingleProject) && !empty($updateSingleProject) && count($updateSingleProject) > 0) {


                    $getProjectAssigneeByProjectId = ProjectRepository::get_project_assignee($updateSingleProject->id);
                    if (isset($getProjectAssigneeByProjectId)) {
                        $projectAssignees = array();
                        foreach ($getProjectAssigneeByProjectId as $key => $value) {
                            array_push($projectAssignees,$getProjectAssigneeByProjectId[$key]->user);
                        }
                        $response['status'] = TRUE;
                        $response['project'] = $updateSingleProject->toArray();
                        $response['assignees'] = $projectAssignees;
                        $response['message'] = "Project update successfully";
                        return response()->json($response, 201);
                    }
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = "Error while updating project please try again.";
                    return response()->json($response, 201);
                }
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Project is not present.";
                return response()->json($response, 201);
            }
        }
    }

    //Function for create single project Detail
    //$request params send by user for create Project
    public function create(Request $request) {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['error' => 'user not found'], 404);
            }
            //validation rule for project create
            $rules = array(
                'name' => 'required',
                'handle' => 'sometimes|unique:projects',
                'category' => 'required',
                'url' => 'url',
            );
            //Check if handle is empty
            //If handle is empty then create random string 
            if ($request->handle == '') {
                //Function create a rendom string
                // $createRendomHandle = ProjectRepository::create_random_handle();

                $createRendomHandle = ProjectRepository::create_project_handle($request->name);
                $request->merge(['handle' => $createRendomHandle]);
            }
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $errors = $validator->getMessageBag()->toArray();
                $response['status'] = FALSE;
                $response['errors'] = $errors;
                $response['message'] = "Some error occure while create .";
                return response()->json($response, 201);
            } else {
                //call function for create single Project
                $createSingleProject = ProjectRepository::create_single_project($request);

                if (isset($createSingleProject) && !empty($createSingleProject) && count($createSingleProject) > 0) {
                    // return response()->json(["a"=>env("APP_AUTOCOLUMN")], 201);                            
                    /* Check env file variable value */
                    if (1 == 1) {
                        /* create predifned status of project, if project created */
                        $statusCreate = StatusRepository::predefined_status($createSingleProject->id);
                    } else {
                        $statusCreate = ['status' => true];
                    }
                    /* If status also created than return tru */
                    if (isset($statusCreate) && !empty($statusCreate) && count($statusCreate) > 0) {
                        $response['status'] = TRUE;
                        $response['project'] = $createSingleProject->toArray();
                        $response['message'] = "Project create successfully.";
                        return response()->json($response, 201);
                    } else {
                        $response['status'] = FALSE;
                        $response['message'] = "Error while creating status of project, please try again.";
                        return response()->json($response, 201);
                    }
                } else {
                    return response()->json('1234', 201);
                    $response['status'] = FALSE;
                    $response['message'] = "Error while creating project please try again.";
                    return response()->json($response, 201);
                }
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //function is use to get project of user
    //params user id
    public function get_projects_by_user_id(Request $request) {
        try {
            $user = JWTAuth::parseToken()->toUser();
            $user_id = $user->id;
            if (isset($user_id) && $user_id != '') {
                $getProject = Project::with('tasks')->where('created_by', '=', $user_id)->get();
                if (isset($getProject) && !empty($getProject) && count($getProject) > 0) {
                    $response['status'] = TRUE;
                    $response['project'] = $getProject->toArray();
                    $response['message'] = "Project  fetch successfully.";
                    return response()->json($response, 200);
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = "Project is not present.";
                    return response()->json($response, 200);
                }
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Something wrong please try again.";
                return response()->json($response, 200);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //Function for get  tasks Detail By project Id
    //get tasks by project id
    //params project id
    public function get_tasks_by_project_id($project_id) {
        try {
            //function check Project Id is present
            $checkProjectIdPresent = ProjectRepository::check_project_present($project_id);
            if ($checkProjectIdPresent) {
                $getProjectTasks = Project::with('tasks')->where('id', '=', $project_id)->get();
                $response['status'] = TRUE;
                $response['project'] = $getProjectTasks->toArray();
                $response['message'] = "Project Tasks fetch successfully.";
                return response()->json($response, 200);
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Project tasks is not present.";
                return response()->json($response, 200);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    /* Function for soft delete a single Project.
     * 
     */

    public function destroy($id) {
        \DB::beginTransaction();
        try {
            $user = JWTAuth::parseToken()->toUser();

            $checkProjectIdPresent = ProjectRepository::check_project_present($id);
            if ($checkProjectIdPresent) {
                $deleteSingleproject = Project::find($id);
                if (isset($deleteSingleproject) && !empty($deleteSingleproject) && count($deleteSingleproject) > 0) {
                    
                    /* array created in a required format to call delete_assignee in taskrepository */
                    /* if $taskData['id'] == "" then itb will delete all records of task */
                    $taskData = array(
                        "id"=>"",
                        "projectId"=>$deleteSingleproject->id
                    );
                    $deleteAssigneeStatus = TaskRepository::delete_assignee($taskData);

                    if($deleteAssigneeStatus){
                     
                        /* Delete all task associated with project */
                        $deleteSingleproject->tasks()->forceDelete();
                        
                        /* Delete all status associated with project */
                        $deleteSingleproject->statuses()->forceDelete();

                        /* Delete all wikis associated with project */
                        $deleteSingleproject->wikis()->forceDelete();

                        /* Delete all credentials associated with project */
                        $deleteSingleproject->credentials()->forceDelete();

                        /* Delete all project assignees associated with project */
                        $deleteSingleproject->assignees()->forceDelete();
                        

                        $deleteSingleproject->forceDelete();

                        \DB::commit();
                        $success = true;
                    } else { 
                        $success = false;
                    }
                } else {
                    $success = false;
                }
                $success = true;
            } else {
                $success = false;
            }
        } catch (\Exception $e) {
            $success = false;
            \DB::rollBack();
        }
        if ($success) {
            $response['status'] = TRUE;
            $response['message'] = "Project deleted successfully.";
            return response()->json($response, 201);
        } else {
            $response['status'] = FALSE;
            $response['message'] = "Error occured wthile deleting project, please try again.";
            return response()->json($response, 201);
        }
    }

    //function for get pagination of project
    public function project_pagination(Request $request) {
        $pagination = ProjectRepository::uims_pagination();
        $response['status'] = FALSE;
        $response['data'] = $pagination;
        return response()->json($pagination, 201);
    }

    //This function is use to change project status
    //params $project_id and status
    public function change_project_status(Request $request, $project_id, $status) {
        //function check Project Id is present
        $checkProjectIdPresent = ProjectRepository::check_project_present($project_id);
        if ($checkProjectIdPresent) {
            //check if project  id and status is present
            if (isset($project_id) && isset($status) && $project_id != '' && $status != '') {
                //check status is present
                $checkStatusPresent = Status::find($status);
                if (isset($checkStatusPresent) && !empty($checkStatusPresent)) {
                    //call repository for change status
                    //this function is use in ProjectRepository.(change_project_status)
                    //this repository return true false.
                    //params is $project_id contain project id
                    //params is $status contain status value to change in database.
                    $changeStatus = ProjectRepository::change_project_status($project_id, $status);
                    //check if status  is change or not.
                    if ($changeStatus) {
                        $response['status'] = TRUE;
                        $response['message'] = "Status change successfully.";
                        return response()->json($response, 200);
                    } else {
                        $response['status'] = FALSE;
                        $response['message'] = "Status is not change please try again.";
                        return response()->json($response, 200);
                    }
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = "Status is not present.";
                    return response()->json($response, 200);
                }
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Something missing please try again.";
                return response()->json($response, 200);
            }
        } else {
            $response['status'] = FALSE;
            $response['message'] = "Project is not Present.";
            return response()->json($response, 200);
        }
    }

    /*
     * Function for get projrect by handle name
     * Params project handle name. 
     */

    public function get_status_by_handle_name(Request $request, $handle_name) {
        try {
//get project Id by handle name
            $getProjectIdByHandleName = Project::where('handle', '=', $handle_name)->first();
            //Check handle is present or not
            if (isset($getProjectIdByHandleName) && isset($getProjectIdByHandleName->id) && !empty($getProjectIdByHandleName) && count($getProjectIdByHandleName) > 0) {
                $getStatusByProjectId = Status::where('project_id', '=', $getProjectIdByHandleName->id)->get();
                if (isset($getStatusByProjectId) && !empty($getStatusByProjectId) && count($getStatusByProjectId) > 0) {
                    $response['status'] = TRUE;
                    $response['data'] = $getStatusByProjectId->toArray();
                    $response['project'] = $getProjectIdByHandleName->toArray();
                    $response['message'] = "Project Status fetch successfully.";
                    return response()->json($response, 200);
                } else {
                    $response['status'] = FALSE;
                    $response['project'] = $getProjectIdByHandleName->toArray();
                    $response['message'] = "Project status not present.";
                    return response()->json($response, 200);
                }
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Project not present.";
                return response()->json($response, 200);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    public function delete_assignee(Request $request){
        if($request->tableName == "task"){
            $deleteTaskAssignee = TaskRepository::delete_assignee($request->all());
            if($deleteTaskAssignee){
                $response['status'] = TRUE;
                $response['message'] = "Assignee is removed from all of its assigned tasks.";
                return response()->json($response, 200);
            }
            else{
                $response['status'] = TRUE;
                $response['message'] = "Some error occured please try again.";
                return response()->json($response, 200);   
            }
        }
        else{
            $deleteProjectAssignee = ProjectRepository::delete_assignee($request->all());
            if($deleteProjectAssignee){
                $response['status'] = TRUE;
                $response['message'] = "Project assignee is removed.";
                return response()->json($response, 200);
            }
            else{
                $response['status'] = TRUE;
                $response['message'] = "Some error occured please try again.";
                return response()->json($response, 200);   
            }   
        }
    }

}
