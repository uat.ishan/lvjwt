<?php

namespace App\Http\Controllers\Api\Comments;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Repositories\Api\Comments\CommentsRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Http\Request;

/**
 * Class CommentsController.
 */

class CommentsController extends Controller {

    public function index() {

    }

    /* Function that will save commment in database from request */
    public function save_comment(Request $request) {
        try{
            $rules = array(
                'message' => 'required'
            );

            /* Validate message field with validator */
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $errors = $validator->getMessageBag()->toArray();
                $response['status'] = FALSE;
                $response['errors'] = $errors;
                $response['message'] = "Some error occure while create.";
                return response()->json($response, 201);
            } else {
                
                /* If message is valid then proceed furthur */
                $saveComment = CommentsRepository::save_comment($request);
                if (isset($saveComment) && !empty($saveComment) && count($saveComment) > 0) {
                    $response['status'] = TRUE;
                    $response['comment'] = $saveComment->toArray();
                    $response['message'] = "Comment created successfully.";
                    return response()->json($response, 201);
                }
                else{
                    $response['status'] = FALSE;
                    $response['message'] = "Error while creating comment please try again.";
                    return response()->json($response, 201);
                }
            }
        }
        catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    public function get_comment_by_id($id){
        
        try{
            
            /* check comment id exist or not */
            $checkComment = CommentsRepository::check_comment($id);
            if(count($checkComment) == 0 && !isset($checkComment) || empty($checkComment)){
                $response['status'] = FALSE;
                $response['message'] = "Comment doesn't exist.";
                return response()->json($response, 201);
            }

            /* Call to repository function that will return single comment from table */
            $comment = CommentsRepository::get_comment_by_id($id);
            if(!empty($comment) && isset($comment) && count($comment) > 0){
                $response['status'] = TRUE;
                $response['comment'] = $comment->toArray();
                $response['message'] = "Comment fetched successfully.";
                return response()->json($response, 201);
            }
            else{
                $response['status'] = FALSE;
                $response['message'] = "No comment found.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    public function get_comments_by_source_id($sourceId){
        
        /* Call to repository function that will return comments depending upon project id */
        $comments = CommentsRepository::get_comments_by_source_id($sourceId);
        if(!empty($comments) && isset($comments) && count($comments) > 0){
            $response['status'] = TRUE;
            $response['comments'] = $comments->toArray();
            $response['message'] = "All Comments fetched successfully.";
            return response()->json($response, 201);
        }
        else{
            $response['status'] = FALSE;
            $response['message'] = "No comment found.";
            return response()->json($response, 201);
        }
    }


    public function get_comments_by_taskid_sourceid(Request $request){
        
        /* Call to repository function that will return comments depending upon project id */
        $comments = CommentsRepository::get_comments_by_taskid_sourceid($request);
        if(!empty($comments) && isset($comments) && count($comments) > 0){
            $commentData = array();
            foreach($comments as $value){
                array_push($commentData,$value);
            }
            // print_r($comments->toArray());die;
            $response['status'] = TRUE;
            $response['comments'] = $commentData;
            $response['message'] = "All Comments fetched successfully.";
            return response()->json($response, 201);
        }
        else{
            $response['status'] = FALSE;
            $response['message'] = "No comment found.";
            return response()->json($response, 201);
        }
    }

}