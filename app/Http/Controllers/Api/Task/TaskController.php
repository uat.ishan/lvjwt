<?php

namespace App\Http\Controllers\Api\Task;

use App\Models\Access\Task\Task;
use App\Models\Access\TaskAssignees\TaskAssignees;
use App\Models\Access\Status\Status;
use App\Http\Controllers\Controller;
use App\Repositories\Api\Task\TaskRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Http\Request;

/**
 * Class TaskController.
 */
class TaskController extends Controller {

    //Function for get all Tasks
    public function index() {
        
    }

    public function get_task_by_project_id($projectId) {
        try {
            $getAllTasks = TaskRepository::get_all_tasks($projectId);

            $response = [
                'status' => FALSE
            ];
            // print_r($getAllTasks);die;
            if (isset($getAllTasks) && count($getAllTasks) > 0) {
                $response['status'] = TRUE;
                $response['tasks'] = $getAllTasks;
                $response['message'] = "Tasks successfully fetched.";

                return response()->json($response, 200);
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Task is not Present.";
                return response()->json($response, 200);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //Function for get single task Detail
    //get task by task id
    //params task id
    public function get_task_by_id($id) {
        try {
            //function check Task Id is present
            $checkTaskIdPresent = TaskRepository::check_task_present($id);
            if ($checkTaskIdPresent) {
                $getSingleTask = Task::with('project')->with('user')->where('id', '=', $id)->get();
                //get task by task id
                $getTaskAssigneeByCreatedTaskId = TaskRepository::get_task_assignee_by_task_id($id);
                if (isset($getTaskAssigneeByCreatedTaskId) && !empty($getTaskAssigneeByCreatedTaskId) && count($getTaskAssigneeByCreatedTaskId) > 0) {
                    //Get Users form task assignee with the help of user id  
                    $assigneeUser = array();
                    foreach ($getTaskAssigneeByCreatedTaskId as $key => $value) {
                        array_push($assigneeUser, $getTaskAssigneeByCreatedTaskId[$key]->user);
                    }
                } else {
                    $getTaskAssigneeByCreatedTaskId = array();
                }
                $getTaskAssigneeByCreatedTaskId['assigneeUsers'] = isset($assigneeUser) ? $assigneeUser : array();

//                $AssigneeData = array(
//                    'assigneeTask' => isset($getTaskAssigneeByCreatedTaskId) ? $getTaskAssigneeByCreatedTaskId : '',
//                );
                $response['status'] = TRUE;
                $response['assigneeUserTask'] = isset($getTaskAssigneeByCreatedTaskId) ? $getTaskAssigneeByCreatedTaskId : array();
                $response['task'] = $getSingleTask->toArray();
                $response['message'] = "Task fetch successfully.";
                return response()->json($response, 200);
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Task is not present.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //Function for update single task Detail
    //$request params send by user for update Task
    public function update(Request $request, $id) {
        try {

            $rules = array(
                'name' => 'required',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $errors = $validator->getMessageBag()->toArray();
                $response['status'] = FALSE;
                $response['errors'] = $errors;
                $response['message'] = "Some error occure while create. ";
                return response()->json($response, 200);
            } else {

                //function check Task Id is present
                $checkTaskIdPresent = TaskRepository::check_task_present($id);
                if ($checkTaskIdPresent) {
                    //call function for update single Task
                    $updateSingleTask = TaskRepository::update_single_task($request, $id);
                    if (isset($updateSingleTask) && !empty($updateSingleTask) && count($updateSingleTask) > 0) {
                        //Function for update assignee with task is

                        $updateAssignee = TaskRepository::update_task_assignee($request, $updateSingleTask->id);
                        if (isset($updateAssignee) && !empty($updateAssignee) && count($updateAssignee) > 0) {
                            //get task by task id
                            $getTaskAssigneeByCreatedTaskId = TaskRepository::get_task_assignee_by_task_id($updateAssignee->task_id);
                            if (isset($getTaskAssigneeByCreatedTaskId) && !empty($getTaskAssigneeByCreatedTaskId) && count($getTaskAssigneeByCreatedTaskId) > 0) {
                                if (isset($getTaskAssigneeByCreatedTaskId->user_id) && $getTaskAssigneeByCreatedTaskId->user_id != '') {
                                    //Get Users form task assignee with the help of user id  
                                    $getUserFormTaskAssignee = TaskRepository::get_user_by_user_id_from_task_assignee($getTaskAssigneeByCreatedTaskId->user_id);
                                    $assigneeUser = isset($getUserFormTaskAssignee) && !empty($getUserFormTaskAssignee) ? $getUserFormTaskAssignee : array();
                                }
                            } else {
                                $getTaskAssigneeByCreatedTaskId = array();
                            }
                            $getTaskAssigneeByCreatedTaskId['assigneeUsers'] = isset($assigneeUser) ? $assigneeUser : array();
                        }
                        $response['status'] = TRUE;
                        $response['task'] = $updateSingleTask->toArray();
                        $response['assigneeTask'] = isset($getTaskAssigneeByCreatedTaskId) ? $getTaskAssigneeByCreatedTaskId : array();
                        $response['message'] = "Task update successfully.";
                        return response()->json($response, 200);
                    } else {
                        $response['status'] = FALSE;
                        $response['message'] = "Error while updating task please try again.";
                        return response()->json($response, 201);
                    }
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = "Task is not present.";
                    return response()->json($response, 201);
                }
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //Function for create single task Detail
    //$request params send by user for create Task
    public function create(Request $request) {
        try {
            //validation rule for task create
            $rules = array(
                'name' => 'required',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $errors = $validator->getMessageBag()->toArray();
                $response['status'] = FALSE;
                $response['errors'] = $errors;
                $response['message'] = "Some error occure while create. ";
                return response()->json($response, 200);
            } else {
                //call function for create single Task
                $createSingleTask = TaskRepository::create_single_task($request);
                if (isset($createSingleTask) && !empty($createSingleTask) && count($createSingleTask) > 0) {
                    //get task by task id
                    $getTaskAssigneeByCreatedTaskId = TaskRepository::get_task_assignee_by_task_id($createSingleTask->id);
                    if (isset($getTaskAssigneeByCreatedTaskId) && !empty($getTaskAssigneeByCreatedTaskId) && count($getTaskAssigneeByCreatedTaskId) > 0) {
                        //Get Users form task assignee with the help loop  
                        $assigneeUser = array();
                        foreach ($getTaskAssigneeByCreatedTaskId as $key => $value) {
                            array_push($assigneeUser, $getTaskAssigneeByCreatedTaskId[$key]->user);
                        }
                    } else {
                        $getTaskAssigneeByCreatedTaskId = array();
                    }
                    $getTaskAssigneeByCreatedTaskId['assigneeUsers'] = isset($assigneeUser) ? $assigneeUser : array();

                    $response['status'] = TRUE;
                    $response['task'] = $createSingleTask->toArray();
                    $response['assigneeTask'] = isset($getTaskAssigneeByCreatedTaskId) ? $getTaskAssigneeByCreatedTaskId : '';
                    $response['message'] = "Task create successfully.";
                    return response()->json($response, 201);
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = "Error while creating task please try again.";
                    return response()->json($response, 201);
                }
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //function for get task by tag name
    //params tag_name
    public function get_tasks_by_tag_name(Request $request) {
        try {
            if (isset($request->tag_name) && $request->tag_name != '') {
                $getTaskByTagName = TaskRepository::get_tasks_by_tag_name($request->tag_name);
                if (isset($getTaskByTagName) && !empty($getTaskByTagName) && count($getTaskByTagName) > 0) {
                    $response['status'] = TRUE;
                    $response['tasks'] = $getTaskByTagName->toArray();
                    $response['message'] = "Tasks  fetch successfully.";
                    return response()->json($response, 200);
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = "No task Present this tag name.";
                    return response()->json($response, 200);
                }
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Please enter Tag name.";
                return response()->json($response, 200);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //function for soft delete a single Task.
    public function destroy($id) {
        try {
            //function check Task Id is present
            $checkTaskIdPresent = TaskRepository::check_task_present($id);
            if ($checkTaskIdPresent) {
                $getTaskAssignees = TaskAssignees::where("task_id", $id)->get();
                if (count($getTaskAssignees) > 0 && !empty($getTaskAssignees) && isset($getTaskAssignees)) {
                    $assigneesId = array();
                    foreach ($getTaskAssignees as $taskAssignee) {
                        array_push($assigneesId, $taskAssignee->user_id);
                    }
                    $deleteStatus = TaskRepository::delete_task_assignee($assigneesId, $id);
                    $deleteAssigneeStatus = $deleteStatus;
                } else {
                    $deleteAssigneeStatus = true;
                }
                if ($deleteAssigneeStatus) {
                    $deleteSingletask = Task::find($id);
                    $deleteSingletask->comments()->forceDelete();
                    if ($deleteSingletask->delete()) {
                        $response['status'] = TRUE;
                        $response['message'] = "Task delete successfully.";
                        return response()->json($response, 200);
                    }
                } else {
                    $response['status'] = False;
                    $response['message'] = "Error occured while deleting tasks assignees.";
                    return response()->json($response, 200);
                }
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Task is not present.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //This function is use to change task status
    //params $task_id and status
    public function change_task_status(Request $request) {
        try {
            //function check Task Id is present
            $checkTaskIdPresent = TaskRepository::check_task_present($request["task_id"]);

            if ($checkTaskIdPresent) {
                //check if task  id and status is present
                if (isset($request["task_id"]) && isset($request["status"]) && $request["task_id"] != '' && $request["status"] != '') {
                    //check status is present
                    $checkStatusPresent = Status::find($request["status"]);
                    if (isset($checkStatusPresent) && !empty($checkStatusPresent)) {
                        //call repository for change status
                        //this function is use in TaskRepository.(change_task_status)
                        //this repository return true false.
                        //params is $task_id contain task id
                        //params is $status contain status value to change in database.
                        $changeStatus = TaskRepository::change_task_status($request->all());
                        //check if status  is change or not.
                        if ($changeStatus) {
                            $response['status'] = TRUE;
                            $response['message'] = "Status change successfully.";
                            return response()->json($response, 200);
                        } else {
                            $response['status'] = FALSE;
                            $response['message'] = "Status is not change please try again.";
                            return response()->json($response, 200);
                        }
                    } else {
                        $response['status'] = FALSE;
                        $response['message'] = "Status is not present.";
                        return response()->json($response, 200);
                    }
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = "Something missing please try again.";
                    return response()->json($response, 200);
                }
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Task is not Present.";
                return response()->json($response, 200);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

}
