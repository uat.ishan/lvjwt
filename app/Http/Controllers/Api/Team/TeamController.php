<?php

namespace App\Http\Controllers\Api\Team;

use App\Models\Access\Team\Team;
use App\Http\Controllers\Controller;
use App\Repositories\Api\Team\TeamRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Http\Request;

/**
 * Class ProfileController.
 */
class TeamController extends Controller {

    //Function for get all teams
    public function index() {
        try {
            $getAllteams = TeamRepository::get_all_teams();
            $response = [
                'status' => FALSE
            ];
            if (isset($getAllteams) && count($getAllteams) > 0) {
                $response['status'] = TRUE;
                $response['teams'] = $getAllteams->toArray();
                $response['message'] = "Teams successfully fetched.";
                return response()->json($response, 201);
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Team is not Present.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //Function for get single team Detail
    public function show($id) {
        try {
            //function check team Id is present
            $checkteamIdPresent = TeamRepository::check_team_present($id);
            if ($checkteamIdPresent) {
                $getSingleteam = Team::with('user')->where('id', '=', $id)->get();
                $response['status'] = TRUE;
                $response['team'] = $getSingleteam->toArray();
                $response['message'] = "Team fetch.";
                return response()->json($response, 201);
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Team is not present.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //Function for update single team Detail
    //$request params send by user for update team
    public function update(Request $request, $id) {
        try {
            //function check team Id is present
            $checkteamIdPresent = TeamRepository::check_team_present($id);
            if ($checkteamIdPresent) {
                //call function for update single team
                $updateSingleteam = TeamRepository::update_single_team($request, $id);
                if (isset($updateSingleteam) && !empty($updateSingleteam) && count($updateSingleteam) > 0) {
                    $response['status'] = TRUE;
                    $response['team'] = $updateSingleteam->toArray();
                    $response['message'] = "Team update successfully";
                    return response()->json($response, 201);
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = "Error while updating team please try again.";
                    return response()->json($response, 201);
                }
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Team is not present.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //Function for create single team Detail
    //$request params send by user for create team
    public function create(Request $request) {
        try {
            //validation rule for team create
            $rules = array(
                'name' => 'required',
                'lead_id' => 'required',
                'user_id' => 'required',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $errors = $validator->getMessageBag()->toArray();
                $response['status'] = FALSE;
                $response['errors'] = $errors;
                $response['message'] = "Some error occure while create .";
                return response()->json($response, 201);
            } else {
                //call function for create single team
                $createSingleteam = TeamRepository::create_single_team($request);
                if (isset($createSingleteam) && !empty($createSingleteam) && count($createSingleteam) > 0) {
                    $response['status'] = TRUE;
                    $response['team'] = $createSingleteam->toArray();
                    $response['message'] = "Team create successfully.";
                    return response()->json($response, 201);
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = "Error while creating team please try again.";
                    return response()->json($response, 201);
                }
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //function is use to get team of user
    //params user id
    public function get_teams_by_user_id($user_id) {
        try {
            if (isset($user_id) && $user_id != '') {
                $getteam = Team::with('user')->where('created_by', '=', $user_id)->get();
                if ($getteam) {
                    $response['status'] = TRUE;
                    $response['team'] = $getteam->toArray();
                    $response['message'] = "Team  fetch successfully.";
                    return response()->json($response, 200);
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = "Team is not present.";
                    return response()->json($response, 200);
                }
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Team is not present.";
                return response()->json($response, 200);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //function for soft delete a single team.
    public function destroy($id) {
        try {
            //function check team Id is present
            $checkteamIdPresent = TeamRepository::check_team_present($id);
            if ($checkteamIdPresent) {
                $deleteSingleteam = Team::find($id);
                if ($deleteSingleteam->delete()) {
                    $response['status'] = TRUE;
                    $response['message'] = "Team delete successfully.";
                    return response()->json($response, 201);
                }
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Team is not present.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

}
