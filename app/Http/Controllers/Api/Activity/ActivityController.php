<?php

namespace App\Http\Controllers\Api\Activity;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Repositories\Api\Activity\ActivityRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Http\Request;

use App\Events\Api\StatusChangeActivityEvent;
use Event;
/**
 * Class AcitivityController.
 */

class ActivityController extends Controller {

    public function index() {

    }

    /* Function that will save wiki in database from request */
    public function save_activity(Request $request) {
         try{

            Event::fire(new StatusChangeActivityEvent($request->all()));
            // $saveActivity = ActivityRepository::save_Activity($request);
            // if (isset($saveActivity) && !empty($saveActivity) && count($saveActivity) > 0) {
            //     $response['status'] = TRUE;
            //     $response['activity'] = $saveActivity->toArray();
            //     $response['message'] = "Activity created successfully.";
            //     return response()->json($response, 201);
            // }
            // else{
            //     $response['status'] = FALSE;
            //     $response['message'] = "Error while creating activity please try again.";
            //     return response()->json($response, 201);
            // }
        }
        catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

}