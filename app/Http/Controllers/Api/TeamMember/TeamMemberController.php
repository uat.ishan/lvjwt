<?php

namespace App\Http\Controllers\Api\TeamMember;

use App\Models\Access\TeamMember\TeamMember;
use App\Http\Controllers\Controller;
use App\Repositories\Api\TeamMember\TeamMemberRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Http\Request;

/**
 * Class ProfileController.
 */
class TeamMemberController extends Controller {

    //Function for get all team members members
    public function index() {
        try {
            $getAllTeamMembers = TeamMemberRepository::get_all_team_members();
            $response = [
                'status' => FALSE
            ];
            if (isset($getAllTeamMembers) && count($getAllTeamMembers) > 0) {
                $response['status'] = TRUE;
                $response['team_members'] = $getAllTeamMembers->toArray();
                $response['message'] = "Team member successfully fetched.";
                return response()->json($response, 201);
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Team Member is not Present.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //Function for get single team members Detail
    public function show($id) {
        try {
            //function check team members Id is present
            $checkTeamMemberIdPresent = TeamMemberRepository::check_team_member_present($id);
            if ($checkTeamMemberIdPresent) {
                $getSingleteamMember = TeamMember::with('user', 'team')->where('id', '=', $id)->get();
                $response['status'] = TRUE;
                $response['team_member'] = $getSingleteamMember->toArray();
                $response['message'] = "Team member fetch.";
                return response()->json($response, 201);
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Team members is not present.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //Function for update single team members Detail
    //$request params send by user for update team members
    public function update(Request $request, $id) {
        try {
            //function check team members Id is present
            $checkTeamMemberIdPresent = TeamMemberRepository::check_team_member_present($id);
            if ($checkTeamMemberIdPresent) {
                //call function for update single team members
                $updateSingleteamMember = TeamMemberRepository::update_single_team_member($request, $id);
                if (isset($updateSingleteamMember) && !empty($updateSingleteamMember) && count($updateSingleteamMember) > 0) {
                    $response['status'] = TRUE;
                    $response['team_member'] = $updateSingleteamMember->toArray();
                    $response['message'] = "Team members update successfully";
                    return response()->json($response, 201);
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = "Error while updating team members please try again.";
                    return response()->json($response, 201);
                }
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Team members is not present.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //Function for create single team members Detail
    //$request params send by user for create team members
    public function create(Request $request) {
        try {
            //validation rule for team members create
            $rules = array(
                'user_id' => 'required',
                'team_id' => 'required',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $errors = $validator->getMessageBag()->toArray();
                $response['status'] = FALSE;
                $response['errors'] = $errors;
                $response['message'] = "Some error occure while create .";
                return response()->json($response, 201);
            } else {
                //call function for create single team members
                $createSingleteamMember = TeamMemberRepository::create_single_team_member($request);
                if (isset($createSingleteamMember) && !empty($createSingleteamMember) && count($createSingleteamMember) > 0) {
                    $response['status'] = TRUE;
                    $response['team member'] = $createSingleteamMember->toArray();
                    $response['message'] = "Team member create successfully.";
                    return response()->json($response, 201);
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = "Error while creating team members please try again.";
                    return response()->json($response, 201);
                }
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //function for soft delete a single team members.
    public function destroy($id) {
        try {
            //function check team members Id is present
            $checkTeamMemberIdPresent = TeamMemberRepository::check_team_member_present($id);
            if ($checkTeamMemberIdPresent) {
                $deleteSingleteamMember = TeamMember::find($id);
                if ($deleteSingleteamMember->delete()) {
                    $response['status'] = TRUE;
                    $response['message'] = "Team member delete successfully.";
                    return response()->json($response, 201);
                }
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Team member is not present.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

}
