<?php

namespace App\Http\Controllers\Api\Status;

use App\Models\Access\Status\Status;
use App\Http\Controllers\Controller;
use App\Repositories\Api\Status\StatusRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Http\Request;

/**
 * Class ProfileController.
 */
class StatusController extends Controller {

    //Function for get all status
    public function index() {
        try {
            $getAllStatuses = StatusRepository::get_all_status();
            $response = [
                'status' => FALSE
            ];
            if (isset($getAllStatuses) && count($getAllStatuses) > 0) {
                $response['status'] = TRUE;
                $response['statuses'] = $getAllStatuses->toArray();
                $response['message'] = "Statuses successfully fetched.";
                return response()->json($response, 201);
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Status is not Present.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //Function for get single status Detail
    public function show($id) {
        try {
            //function check status Id is present
            $checkstatusIdPresent = StatusRepository::check_status_present($id);
            if ($checkstatusIdPresent) {
                $getSingleStatus = Status::find($id);
                $response['status'] = TRUE;
                $response['data'] = $getSingleStatus->toArray();
                $response['message'] = "Status fetch successfully.";
                return response()->json($response, 200);
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Status is not present.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //Function for update single status Detail
    //$request params send by user for update status
    public function update(Request $request, $id) {
        try {
            //function check status Id is present
            $checkstatusIdPresent = StatusRepository::check_status_present($id);
            if ($checkstatusIdPresent) {
                //call function for update single status
                $updateSinglestatus = StatusRepository::update_single_status($request, $id);
                if (isset($updateSinglestatus) && !empty($updateSinglestatus) && count($updateSinglestatus) > 0) {
                    $response['status'] = TRUE;
                    $response['data'] = $updateSinglestatus->toArray();
                    $response['message'] = "Status update successfully";
                    return response()->json($response, 201);
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = "Error while updating status please try again.";
                    return response()->json($response, 201);
                }
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Status is not present.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //Function for create single status Detail
    //$request params send by user for create status
    public function create(Request $request) {
        try {
            //validation rule for status create
            $rules = array(
                'status_name' => 'required',
                'user_id' => 'required',
                'status_id' => 'required',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $errors = $validator->getMessageBag()->toArray();
                $response['status'] = FALSE;
                $response['errors'] = $errors;
                $response['message'] = "Some error occure while create .";
                return response()->json($response, 201);
            } else {
                //call function for create single status
                $createSinglestatus = StatusRepository::create_single_status($request);
                if (isset($createSinglestatus) && !empty($createSinglestatus) && count($createSinglestatus) > 0) {
                    $response['status'] = TRUE;
                    $response['data'] = $createSinglestatus->toArray();
                    $response['message'] = "Status create successfully.";
                    return response()->json($response, 201);
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = "Error while creating status please try again.";
                    return response()->json($response, 201);
                }
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    //function for soft delete a single status.
    public function destroy($id) {
        try {
            //function check status Id is present
            $checkstatusIdPresent = StatusRepository::check_status_present($id);
            if ($checkstatusIdPresent) {
                $deleteSinglestatus = status::find($id);
                if ($deleteSinglestatus->delete()) {
                    $response['status'] = TRUE;
                    $response['message'] = "Status delete successfully.";
                    return response()->json($response, 201);
                }
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Status is not present.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    /*
     * Function for get last git commit...
     */

    public function getLastCommit() {
        try {
            $rev = exec('git show --oneline -s');
            if (isset($rev) && $rev != '') {
                $commitId = strtok($rev, " "); // Id
                $commitMessage = str_replace($commitId, '', $rev);
                $commitData = array(
                    'commit_id' => isset($commitId) && $commitId != '' ? $commitId : '',
                    'commit_message' => isset($commitMessage) && $commitMessage != '' ? trim($commitMessage) : ''
                );
                $response['status'] = TRUE;
                $response['errors'] = FALSE;
                $response['commit_data'] = $commitData;
                return response()->json($response, 200);
            } else {
                $response['status'] = FALSE;
                $response['errors'] = TRUE;
                $response['message'] = 'Enable fetch  commit.';
                return response()->json($response, 200);
            }
        } catch (\Exception $ex) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $ex->getMessage();
            return response()->json($response, 200);
        }
    }

}
