<?php

namespace App\Http\Controllers\Api\Board;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Repositories\Api\Board\BoardRepository;
use App\Repositories\Api\Task\TaskRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Access\Task\Task;
use App\Models\Access\Status\Status;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Http\Request;

/* Class BoardController */

class BoardController extends Controller {
    /* Function to delete status board and and subtask inside board */

    public function delete_board($statusId) {
        $taskFindStatus = Task::where("status", "=", $statusId)->get();
        if (isset($taskFindStatus) && !empty($taskFindStatus) && count($taskFindStatus) > 0) {
            \DB::beginTransaction();
            /* Firstly delete the board requested as ID, id deletion successfull than delete subtasks */
            try {
                $deleteSinglestatus = status::find($statusId);
                if (isset($deleteSinglestatus) && !empty($deleteSinglestatus) && count($deleteSinglestatus) > 0) {
                    $deleteSinglestatus->delete();
//                    throw new \Exception('scdsfsdf');
                    $deleteTask = Task::where("status", "=", $statusId)->delete();
                    \DB::commit();
                    $success = true;
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = "Status is not present.";
                    return response()->json($response, 201);
                }
            } catch (\Exception $e) {

                $success = false;
                //If there is an error/exception in the above code before commit, it'll rollback
                \DB::rollBack();
            }
            if ($success) {
                $response['status'] = TRUE;
                $response['message'] = "Board deleted successfully.";
                return response()->json($response, 200);
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Error occured while deleting tasks of status.. ";
                return response()->json($response, 201);
            }
        } else {
            $deleteSinglestatus = status::find($statusId);
            if (isset($deleteSinglestatus) && !empty($deleteSinglestatus) && count($deleteSinglestatus) > 0) {
                $deleteSinglestatus->delete();
                $response['status'] = TRUE;
                $response['message'] = "Board deleted successfully.";
                return response()->json($response, 200);
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Status is not present.";
                return response()->json($response, 201);
            }
        }
    }

}
