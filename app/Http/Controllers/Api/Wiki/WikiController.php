<?php

namespace App\Http\Controllers\Api\Wiki;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Repositories\Api\Wiki\WikiRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Http\Request;

/**
 * Class CommentsController.
 */

class WikiController extends Controller {

    public function index() {

    }

    /* Function that will save wiki in database from request */
    public function save_wiki(Request $request) {
        try{
            $rules = array(
                'data' => 'required'
            );

            /* Validate datafield with validator */
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $errors = $validator->getMessageBag()->toArray();
                $response['status'] = FALSE;
                $response['errors'] = $errors;
                $response['message'] = "Some error occure while create.";
                return response()->json($response, 201);
            } else {
                
                /* If data is valid then proceed furthur */
                $saveWiki = WikiRepository::save_wiki($request);
                if (isset($saveWiki) && !empty($saveWiki) && count($saveWiki) > 0) {
                    $response['status'] = TRUE;
                    $response['wiki'] = $saveWiki->toArray();
                    $response['message'] = "Wiki created successfully.";
                    return response()->json($response, 201);
                }
                else{
                    $response['status'] = FALSE;
                    $response['message'] = "Error while creating wiki please try again.";
                    return response()->json($response, 201);
                }
            }
        }
        catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    public function get_wiki_by_projectId($projectId){
        
        try{
            
            /* Call to repository function that will return single comment from table */
            $wiki = WikiRepository::get_wiki_by_projectId($projectId);
            
            if(!empty($wiki) && isset($wiki) && count($wiki) > 0){
                $response['status'] = TRUE;
                $response['wiki'] = $wiki->toArray();
                $response['message'] = "Wiki fetched successfully.";
                return response()->json($response, 201);
            }
            else{
                $response['status'] = FALSE;
                $response['message'] = "No wiki found.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

}