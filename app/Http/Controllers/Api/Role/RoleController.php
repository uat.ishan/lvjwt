<?php

namespace App\Http\Controllers\Api\Role;

use App\Models\Access\Role\Role;
use App\Http\Controllers\Controller;
use App\Repositories\Api\Role\RoleRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Http\Request;

/**
 * Class RoleController.
 */
class RoleController extends Controller {
    
    /* Function to get all roles */
    public function index() {
        $getAllRoles = RoleRepository::get_all_roles();
        $response = [
            'status' => FALSE
        ];
        if (isset($getAllRoles) && count($getAllRoles) > 0) {
            $response['status'] = TRUE;
            $response['roles'] = $getAllRoles->toArray();
            $response['message'] = "Roles successfully fetched.";
        } else {
            $response['status'] = FALSE;
            $response['message'] = "No Role Found.";
        }
        return response()->json($response, 200);
    }
    
    /* Function to get role only if true returned from repository */
    public function show($id) {
        
        /* function to check Role Id is present in database or not */
        $checkRoleIdPresent = RoleRepository::check_role_present($id);
        if ($checkRoleIdPresent) {
            $getSingleRole = Role::find($id);
            $response['status'] = TRUE;
            $response['role'] = $getSingleRole->toArray();
            $response['message'] = "Single Role fetched.";
            return response()->json($response, 201);
        } else {
            $response['status'] = FALSE;
            $response['message'] = "Role doesn't exist.";
            return response()->json($response, 201);
        }
    }
    
    /* Function to update role whose id exist in database and id exist details sent in request will be saved or updated*/
    public function update(Request $request, $id) {
        //function check Project Id is present
        $checkRoleIdPresent = RoleRepository::check_role_present($id);
        if ($checkRoleIdPresent) {
            //call function for update single Project
            $updateSingleRole = RoleRepository::update_single_role($request, $id);
            if (isset($updateSingleRole) && !empty($updateSingleRole) && count($updateSingleRole) > 0) {
                $response['status'] = TRUE;
                $response['role'] = $updateSingleRole->toArray();
                $response['message'] = "Role update successfully.";
                return response()->json($response, 200);
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Error occured while updating role, please try again.";
                return response()->json($response, 201);
            }
        } else {
            $response['status'] = FALSE;
            $response['message'] = "Role not found.";
            return response()->json($response, 201);
        }
    }
    
    /* Function to create a new role, $request will contain all details regarding new role */
    public function create(Request $request) {
        
        /* Validations to validate role details before insering */
        $rules = array(
            'name' => 'required',
            'all' => 'required',
            'sort' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->getMessageBag()->toArray();
            $response['status'] = FALSE;
            $response['errors'] = $errors;
            $response['message'] = "Some error occure while creating role, please try again.";
            return response()->json($response, 201);
        } else {
            $createSingleRole = RoleRepository::create_single_role($request);
            if (isset($createSingleRole) && !empty($createSingleRole) && count($createSingleRole) > 0) {
                $response['status'] = TRUE;
                $response['role'] = $createSingleRole->toArray();
                $response['message'] = "Role created successfully.";
                return response()->json($response, 200);
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Error occured while creating new role, please try again.";
                return response()->json($response, 201);
            }
        }
    }
    
    /* Fucntion to delete role after checking its existence */
    public function destroy($id) {
        $checkRoleIdPresent = RoleRepository::check_role_present($id);
        if ($checkRoleIdPresent) {
            $deleteSingleRole = Role::find($id);
            if ($deleteSingleRole->delete()) {
                $response['status'] = TRUE;
                $response['message'] = "Role deleted successfully.";
                return response()->json($response, 200);
            }
        } else {
            $response['status'] = FALSE;
            $response['message'] = "Role not found.";
            return response()->json($response, 201);
        }
    }
}
