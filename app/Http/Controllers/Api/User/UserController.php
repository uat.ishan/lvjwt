<?php

namespace App\Http\Controllers\Api\User;

use App\Models\Access\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\RegisterRequest;
use App\Repositories\Api\User\UserRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Http\Request;

/**
 * Class ProfileController.
 */
class UserController extends Controller {

    public function index() {
        $response = [
            'status' => false
        ];
        try {
            $users = UserRepository::get_users();
            if (isset($users) && count($users) > 0) {
                $response['status'] = TRUE;
                $response['users'] = $users->toArray();
                return response()->json($response, 201);
            } else {
                $response['users'] = [];
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    /**
     * 
     * Function to register a user
     * 
     * */
    public function register(RegisterRequest $request) {
        try {
            // validate form data need to move this to request folder
            $validator = Validator::make($request->all(), [
                        'first_name' => 'required|string|max:191',
                        'last_name' => 'required|string|max:191',
                        'email' => ['required', 'string', 'email', 'max:191', Rule::unique('users')],
                        'password' => 'required|string|min:6|confirmed'
            ]);
            if ($validator->fails()) {
                $errors = $validator->getMessageBag()->toArray();
                return response()->json(array(
                            'status' => FALSE,
                            'errors' => $errors
                ));
            }
            $user = new User();
            $userData = $request->all();
            $userData["password"] = bcrypt($userData["password"]);
            $user->fill($userData);
            $response = [
                'status' => FALSE
            ];
            if ($user->save()) {
                $response['status'] = TRUE;
                $response['user'] = $user->toArray();
                $response['message'] = "congratulation your account has been created.";
                return response()->json($response, 201);
            } else {
                $response['message'] = "Something went wrong please try again.";
                return response()->json($response, 501);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    /**
     * 
     * Function to login a user
     * 
     * */
    public function signIn(RegisterRequest $request) {
//        print_r($request->all());die();
        try {
            // validate form data need to move this to request folder
            $validator = Validator::make($request->all(), [
                        'email' => 'required|email',
                        'password' => 'required|min:4'
            ]);
            if ($validator->fails()) {
                $errors = $validator->getMessageBag()->toArray();
                return response()->json(array(
                            'status' => FALSE,
                            'errors' => $errors
                ));
            }
            $credentials = $request->only('email', 'password');
            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return response()->json([
                                'status' => FALSE,
                                'message' => 'Invalid credentials.'
                                    ], 401);
                }
            } catch (JWTException $e) {
                return response()->json([
                            'status' => FALSE,
                            'message' => 'Could not create credentials.'
                                ], 500);
            }

            return response()->json([
                        'status' => TRUE,
                        '_JWTtoken' => $token
                            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
            'status' => TRUE,
            'error' => $ex->getMessage(),
            ], 200);
        }
    }

    public function logout($token) {
        JWTAuth::invalidate($token);
        $response['status'] = TRUE;
        $response['message'] = "logout Successfull";
    }

    public function get_user() {
        try {
            $user = JWTAuth::parseToken()->toUser();
            $user = UserRepository::get_single_user($user->id);
            $response = [
                'status' => false
            ];
            if (isset($user) && count($user) > 0) {
                $response['status'] = TRUE;
                $response['user'] = $user;
                return response()->json($response, 201);
            } else {
                $response['user'] = [];
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    public function update_user(Request $request) {
        try {
            $user = JWTAuth::parseToken()->toUser();
            //call function for update single User
            $updateSingleUser = UserRepository::update_user($request, $user->id);
            if (isset($updateSingleUser) && !empty($updateSingleUser) && count($updateSingleUser) > 0) {
                $response['status'] = TRUE;
                $response['user'] = $updateSingleUser->toArray();
                $response['message'] = "Profile Updated Successfully!!";
                return response()->json($response, 201);
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Error while updating user please try again.";
                return response()->json($response, 201);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    public function check_password(Request $request) {
        try {
            $user = JWTAuth::parseToken()->toUser();
            //call function for check current password of User
            $checkPassword = UserRepository::check_password($request->password);
            if (isset($checkPassword) && !empty($checkPassword) && $checkPassword) {
                $response['status'] = TRUE;
                $response['message'] = "Password matched with current password.";
                return response()->json($response, 200);
            } else {
                $response['status'] = FALSE;
                $response['message'] = "Enterted password not matches with current password.";
                return response()->json($response, 200);
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    public function change_password(Request $request) {
        try {
            $user = JWTAuth::parseToken()->toUser();
            $validator = Validator::make($request->all(), [
                        'password' => 'required|string|min:6|confirmed'
            ]);
            if ($validator->fails()) {
                $errors = $validator->getMessageBag()->toArray();
                return response()->json(array(
                            'status' => FALSE,
                            'errors' => $errors
                ));
            } else {
                $current_password = UserRepository::check_password($request->current_password);
                if (isset($current_password) && !empty($current_password) && $current_password) {
                    $changePassword = UserRepository::change_password($request);
                    if (isset($changePassword) && !empty($changePassword) && $changePassword) {
                        $response['status'] = TRUE;
                        $response['message'] = "Password changed !!";
                        return response()->json($response, 200);
                    } else {
                        $response['status'] = FALSE;
                        $response['message'] = "Error occured while password changed !!";
                        return response()->json($response, 200);
                    }
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = "Enterted password not matches with current password.";
                    return response()->json($response, 200);
                }
            }
        } catch (\Exception $e) {
            $response['status'] = FALSE;
            $response['errors'] = TRUE;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }

    public function send_reset_link_email(Request $request) {
        $validator = Validator::make($request->all(), [
                    'email' => 'required|email'
        ]);
        if ($validator->fails()) {
            $errors = $validator->getMessageBag()->toArray();
            return response()->json(array(
                        'status' => FALSE,
                        'errors' => $errors
            ));
        } else {
            // We will send the password reset link to this user. Once we have attempted
            // to send the link, we will examine the response then see the message we
            // need to show to the user. Finally, we'll send out a proper response.
            $response = $this->broker()->sendResetLink(
                    $request->only('email')
            );

            return $response == Password::RESET_LINK_SENT ? $this->sendResetLinkResponse($response) : $this->sendResetLinkFailedResponse($request, $response);
        }
    }

    public function broker() {
        return Password::broker();
    }

}
