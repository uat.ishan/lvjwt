<?php

namespace App\Models\Access\Wikis;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Wikis.
 */

class Wikis extends Authenticatable {

    // use SoftDeletes;
    protected $table = "wiki";
    protected $dates = ['deleted_at'];
    protected $fillable = ['id', 'project_id', 'user_id', 'data', 'deleted_at', 'created_at', 'updated_at'];    

}
