<?php

namespace App\Models\Access\ProjectAssignees;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User.
 */
class ProjectAssignees extends Authenticatable {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'project_assignees';
    protected $fillable = ['id', 'user_id', 'project_id', 'created_by', 'deleted_at', 'created_at', 'updated_at'];

    /*
     * Assignee table  association with user table.
     */
    public function user() {
        return $this->hasOne('App\Models\Access\User\User', 'id', 'user_id');
    }
    
    public function project() {
        return $this->hasOne('App\Models\Access\Project\Project', 'id', 'project_id');
    }
}
