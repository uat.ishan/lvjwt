<?php

namespace App\Models\Access\Status;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User.
 */
class Status extends Authenticatable {

    // use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $fillable = ['id', 'status_name', 'user_id', 'status_id', 'project_id','deleted_at', 'created_at', 'updated_at'];

    /* Association for task with status
     * 
     */

    public function tasks() {
        return $this->hasMany('App\Models\Access\Task\Task', 'status', 'status_id');
    }
    

}
