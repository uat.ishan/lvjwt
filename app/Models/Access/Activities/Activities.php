<?php

namespace App\Models\Access\Activities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Activities.
 */

class Activities extends Authenticatable {

    // use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['id', 'type_id', 'source_id', 'created_by', 'data', 'deleted_at', 'created_at', 'updated_at'];

    /* Association with user model */
    public function user() {
        return $this->hasOne('App\Models\Access\User\User', 'id', 'created_by');
    }    

}
