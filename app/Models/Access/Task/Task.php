<?php

namespace App\Models\Access\Task;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Access\TaskAssignees\TaskAssignees;
use App\Models\Access\Project\Project;
use JWTAuth;

/**
 * Class User.
 */
class Task extends Authenticatable {

    // use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    
    protected $fillable = ['name', 'tag_name', 'project_id','description', 'status' ,' end_date ','created_by', 'deleted_at', 'created_at', 'updated_at'];

    protected $appends = ['edit_check'];
    
    public function project() {
        return $this->hasOne('App\Models\Access\Project\Project', 'id', 'project_id');
    }

    public function user() {
        return $this->hasOne('App\Models\Access\User\User', 'id', 'created_by');
    }
    
    public function statuses() {
        return $this->hasMany('App\Models\Access\Status\Status', 'status_id', 'status');
    }

    public function comments() {
        return $this->hasMany('App\Models\Access\Comments\Comments', 'source_id', 'id');
    }

    /* function that will set value of extra parameter edit_check */
    public function getEditCheckAttribute() {

        $user = JWTAuth::parseToken()->toUser();

        $checkProjectCreator = Project::where("created_by",$user->id)->get();
        if(count($checkProjectCreator)>0 && !empty($checkProjectCreator) && isset($checkProjectCreator)){
            return 1;
        }

        if($this->created_by == $user->id)
            return 1;

        /* cehck if task assigned to current user than set status to 1 else set status 0 */
        $taskAssignee = TaskAssignees::where("user_id",$user->id)->where("task_id",$this->id)->get();
        $status = 1;
        if(count($taskAssignee)>0 && !empty($taskAssignee) && isset($taskAssignee)){
            $status = 1;
        } else {
            $status = 0;
        }
        return $status;
    }
    
}
