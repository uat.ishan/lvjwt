<?php

namespace App\Models\Access\Project;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User.
 */
class Project extends Authenticatable {

    // use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    
    protected $fillable = ['handle', 'name', 'description', 'assignee', 'url', 'category', 'project_lead', 'tag_name', 'status','privacy_status','created_by', 'created_at', 'updated_at'];

    
    public function tasks() {
        return $this->hasMany('App\Models\Access\Task\Task', 'project_id', 'id');
    }
    
    public function statuses() {
        return $this->hasMany('App\Models\Access\Status\Status', 'project_id', 'id');
    }

    public function wikis() {
        return $this->hasOne('App\Models\Access\Wikis\Wikis', 'project_id', 'id');
    }

    public function credentials() {
        return $this->hasOne('App\Models\Access\Credentials\Credentials', 'project_id', 'id');
    }

    public function assignees() {
        return $this->hasMany('App\Models\Access\ProjectAssignees\ProjectAssignees', 'project_id', 'id');
    }
    
}
