<?php

namespace App\Models\Access\Team;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User.
 */
class Team extends Authenticatable {

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $fillable = [ 'id', 'name', 'lead_id', 'user_id', 'created_by', 'deleted_at', 'created_at', 'updated_at'];

    public function user() {
        return $this->hasMany('App\Models\Access\User\User', 'id', 'user_id');
    }
}
