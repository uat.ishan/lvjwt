<?php

namespace App\Models\Access\Credentials;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Credentials.
 */

class Credentials extends Authenticatable {

    // use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['id', 'project_id', 'user_id', 'data', 'deleted_at', 'created_at', 'updated_at'];    

}
