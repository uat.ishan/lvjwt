<?php

namespace App\Models\Access\ActivityType;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class ActivityType.
 */

class ActivityType extends Authenticatable {


    protected $table = 'activity_type';

    protected $dates = ['deleted_at'];
    protected $fillable = ['id', 'name', 'created_by'];    

}
