<?php

namespace App\Models\Access\Comments;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Comments.
 */

class Comments extends Authenticatable {

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['id', 'type_id', 'user_id', 'message', 'source_id','deleted_at', 'created_at', 'updated_at'];    

    /* Association with project model */
    public function task() {
        return $this->hasOne('App\Models\Access\Task\Task', 'id', 'source_id');
    }

    /* Association with user model */
    public function user() {
        return $this->hasOne('App\Models\Access\User\User', 'id', 'user_id');
    }
}
