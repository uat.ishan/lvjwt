<?php

namespace App\Repositories\Api\Activity;

use App\Models\Access\Activities\Activities;
use App\Models\Access\User\User;
use App\Models\Access\ActivityType\ActivityType;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use JWTAuth;
use Carbon\Carbon;

/**
 * Class ActivityRepository.
 */
class ActivityRepository extends BaseRepository {

    public static function get_last_five_activities($commentDetails) {
		$getActivities;

		/* Following condition is to check case if any comment of source is present or not */
    	if($commentDetails["comments"] != ""){
	        $getActivities = Activities::where("created_at", ">=", $commentDetails["startDate"]->toDateString())->where("source_id", $commentDetails["sourceId"])->where("type_id",$commentDetails["typeId"])->with("user")->get();
		    if(isset($getActivities) && count($getActivities)>0 && !empty($getActivities)){
		        $comment_activity_data = $getActivities->merge($commentDetails["comments"]);
		        
		        $comment_activity_data = array_merge($getActivities->toArray(),$commentDetails["comments"]->toArray());
		        $sortedActivityData = array_values(array_sort($comment_activity_data,function($value){
		        	            return $value["created_at"];
		        	        }));
		        return array_reverse($sortedActivityData);
	        } else {
	        	return array();
	        }
	    } else {

	    	/* If source has not comments then return latest activities */
	    	$date = Carbon::now();
	    	$date->subDays($commentDetails["days"]);
	    	$getActivities = Activities::where('created_at', '>', $date->toDateTimeString())->where("source_id", $commentDetails["sourceId"])->where("type_id",$commentDetails["typeId"])->with("user")->get();
		    if(isset($getActivities) && count($getActivities)>0 && !empty($getActivities)){
		    	return array_reverse($getActivities->toArray());
		    } else {
		    	return array();
		    }
	    }
    }

}
