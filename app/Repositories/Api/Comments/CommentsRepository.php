<?php

namespace App\Repositories\Api\Comments;
use App\Repositories\Api\Activity\ActivityRepository;
use App\Models\Access\Comments\Comments;
use App\Models\Access\CommentType\CommentType;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use JWTAuth;
use Carbon\Carbon;
/**
 * Class UserRepository.
 */
class CommentsRepository extends BaseRepository {

    const MODEL = Comments::class;

    /* function that will check wether comment exist in table or not */
    public static function check_comment($id){
        $comment = Comments::find($id)->first();
        if(count($comment)>0 && isset($comment) && !empty($comment)){
            return $comment;
        } else{
            return array();
        }
    }

    /* Function to save records in comment table */
    public static function save_comment($request){

        /* get user details from jwt token */
        $user = JWTAuth::parseToken()->toUser();

        /* get record from comment_type table regarding type sent in request */
        $commentTypeData = CommentType::where("name",$request->type)->first();
        
        if(count($commentTypeData)>0 && !empty($commentTypeData)){
            $commentObj = new Comments();   
            $commentObj->fill($request->all());
            $commentObj->user_id = $user->id;
            $commentObj->type_id = $commentTypeData->id;
            if ($commentObj->save()) {

                $comment = Comments::where("id",$commentObj->id)->with("task")->with("user")->first();
                return $comment;
            } else {
                return false;
            }
        }
    }

    /* Function to get single record from comments table */
    public static function get_comment_by_id($id){
        
        $comment = Comments::find($id)->with("task")->with("user")->first();
        if(!empty($comment) && isset($comment) && count($comment) > 0){
            return $comment;
        } else {
            return array();
        }
    }

    /* Function to get comments according to project from comments table */
    public static function get_comments_by_source_id($sourceId){
        
        $comments = Comments::where("source_id",$sourceId)->with("task")->with("user")->orderBy('id', 'desc')->get();
        if(!empty($comments) && isset($comments) && count($comments) > 0){
            return $comments;
        } else {
            return array();
        }
    }

    /* Function to get comments according to project and type from comments table */
    public static function get_comments_by_taskid_sourceid($request){

        $commentData = self::get_comments_five_days($request);
        return $commentData;
    }
    
    public static function get_comments_five_days($request){
        $comment_activity_data;
        $commentTypeData = CommentType::where("name",$request->type)->first();

        /* Check if comment type is fetched succesfully */
        if(isset($commentTypeData) && !empty($commentTypeData) && count($commentTypeData) > 0){
            $date = Carbon::now();

            /* number of days record is to be fetched is sent in request */
            $date->subDays($request->days);
            $commentData = Comments::where("source_id",$request->source_id)->where("type_id",$commentTypeData->id)->where('created_at', '>=', $date->toDateString())->with("user")->orderBy('id', 'desc')->get();
            $commentDetails = array();

            /* If comments are present for source then set array with details and pass to repository of activities */

            if(count($commentData)>0 && isset($commentData) && !empty($commentData)){

                $startDate = $commentData[0]->created_at;
                $endDate = $commentData[count($commentData)-1]->created_at;
                $commentDetails = array(
                    'typeId' => $commentTypeData->id,
                    'sourceId' => $request->source_id,
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                    'comments' => $commentData
                );
                $comment_activity_data = ActivityRepository::get_last_five_activities($commentDetails);
            } else {

                /* If no comment is present then pass array with type and source id only */
                $commentDetails = array(
                    'typeId' => $commentTypeData->id,
                    'sourceId' => $request->source_id,
                    'startDate' => "",
                    'endDate' => "",
                    'comments' => "",
                    'days' => $request->days
                );

                $comment_activity_data = ActivityRepository::get_last_five_activities($commentDetails);
            }

            /* If comments/activity fetched then return, else return empty array */
            if(count($comment_activity_data) > 0 && isset($comment_activity_data) && !empty($comment_activity_data)){
                return $comment_activity_data;
            } else {
                return $commentData;
            }
        }
        
    }

}
