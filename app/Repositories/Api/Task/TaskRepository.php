<?php

namespace App\Repositories\Api\Task;

use App\Models\Access\Task\Task;
use App\Models\Access\Status\Status;
use App\Models\Access\TaskAssignees\TaskAssignees;
use App\Models\Access\ProjectAssignees\ProjectAssignees;
use App\Models\Access\Project\Project;
use App\Models\Access\User\User;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Events\Api\TaskAssigneeAddEvent;
use App\Events\Api\TaskAssigneeRemoveEvent;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use JWTAuth;
use Event;

/**
 * Class UserRepository.
 */
class TaskRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Task::class;

    /**
     * @param RoleRepository $role
     */
    //function for get all tasks
    public static function get_all_tasks($projectId) {

        $user = JWTAuth::parseToken()->toUser();
        $project = Project::where("id", "=", $projectId)->where('created_by', $user->id)->get();
        $projectData = Project::where("id", "=", $projectId)->first();

        if (isset($project) && count($project) > 0) {


            $allTasks = Task::where('project_id', $project[0]->id)->with("user")->orderBy('order', 'asc')->get();
            $allTasks = ($allTasks && count($allTasks) > 0) ? $allTasks->toArray() : [];
        } else if ($projectData->privacy_status == 0) {
            $allTasks = Task::where('project_id', $projectData->id)->orderBy('order', 'asc')->with("user")->get();
            $allTasks = ($allTasks && count($allTasks) > 0) ? $allTasks->toArray() : [];
        } else {
            /* Check if user is assigned to this project, if assigned then return all tasks */
            $checkUserAssignedToProject = ProjectAssignees::where("user_id", "=", $user->id)->get();

            if (isset($checkUserAssignedToProject) && count($checkUserAssignedToProject) > 0 && !empty($checkUserAssignedToProject)) {

                $allTasks = Task::where('project_id', $projectId)->orderBy('order', 'asc')->with("user")->get();
            } else {

                $allAssignedTasks = TaskAssignees::with('task.user')->with("user")->where("user_id", "=", $user->id)->get();
                $allTasks = [];
                foreach ($allAssignedTasks as $task) {
                    if ($task->task->project_id == $projectId) {
                        $allTasks[] = $task->task;
                    }
                }
                $currentUserTask = Task::where("created_by", "=", $user->id)->orderBy('order', 'asc')->with("user")->get();
                // print_r($allAssignedTasks);die;
                if (isset($currentUserTask) && count($currentUserTask) > 0) {
                    foreach ($currentUserTask as $key => $value) {
                        array_push($allTasks, $value);
                    }
                }
            }
        }

        if (isset($allTasks) && !empty($allTasks) && count($allTasks) > 0) {
            return $allTasks;
        } else {
            return array();
        }
    }

    //function for check task is present or not
    //if task is present then return task array
    //else return false
    //param $id is task Id

    public static function check_task_present($id) {
        $checkTaskPresent = Task::find($id);
        if (isset($checkTaskPresent) && !empty($checkTaskPresent) && count($checkTaskPresent) > 0) {
            return true;
        } else {
            return false;
        }
    }

    //function for check task is present or not
    //if task is present then return task array
    //else return false
    //param $id is task Id

    public static function check_project_present($project_id) {
        $checkProjectPresent = Task::where('project_id', '=', $project_id)->get();
        if (isset($checkProjectPresent) && !empty($checkProjectPresent) && count($checkProjectPresent) > 0) {
            return true;
        } else {
            return false;
        }
    }

    //function for get task by tag name
    //params tag name
    public static function get_tasks_by_tag_name($tag_name) {
        $getTasks = Task::where('tag_name', '=', $tag_name)->get();
        if (isset($getTasks) && !empty($getTasks) && count($getTasks) > 0) {
            return $getTasks;
        } else {
            return array();
        }
    }

    //function for update single task
    //param $id is task Id
    public static function update_single_task($request, $id) {
        //call function for check task is Present
        $checkTaskPresent = self::check_task_present($id);
        if ($checkTaskPresent) {
            //create object of task for update task Detail
            $updateSingleTask = Task::find($id);
            if ($request->end_date && $request->end_date != "") {
                $newformat = date('Y-m-d H:i:s', strtotime($request->end_date));
                $updateSingleTask->end_date = $newformat;
            }
            $updateSingleTask->fill($request->all());
            if ($updateSingleTask->save()) {
                $taskData = Task::where("id", $updateSingleTask->id)->orderBy('order', 'asc')->with("user")->first();
                return $taskData;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //function for create single task
    //param $request is task detail to be save
    public static function create_single_task($request) {
        //Get task by project id and status id order by oreder (desc) 
        $getOrder = Task::where('project_id', '=', $request->project_id)->where('status', '=', $request->status)->orderBy('order', 'desc')->take(1)->first();
        if (isset($getOrder) && !empty($getOrder) && count($getOrder) > 0) {
            $order = $getOrder->order + 1;
        } else {
            $order = 1;
        }
//        $getOrderCount = $getOrder->order + 1;
        $user = JWTAuth::parseToken()->toUser();        //create object of task for update task Detail
        $createSingleTask = new Task;
        $createSingleTask->fill($request->all());
        $createSingleTask->created_by = $user->id;
        $createSingleTask->order = isset($order) ? $order : '';
        if ($request->end_date && $request->end_date != "") {
            $newformat = date('Y-m-d H:i:s', strtotime($request->end_date));
            $createSingleTask->end_date = $newformat;
        }
        if ($createSingleTask->save()) {
            $assignTaskDetails = array(
                "task_id" => $createSingleTask->id,
                "user_id" => $request->user_id,
                "created_by" => $user->id,
            );
            $assignTaskStatus = self::assign_task($assignTaskDetails);
            if ($assignTaskStatus) {
                $singleTask = Task::where("id", $createSingleTask->id)->with("project")->with("user")->first();
                return $singleTask;
            }
        } else {
            return false;
        }
    }

    public static function assign_task($request) {
        try {
            $user = JWTAuth::parseToken()->toUser();
            if ($request["user_id"] != null || $request["user_id"] != "") {
                $userIdArray = explode(",", $request["user_id"]);
                $status = false;
                $newUsers = array();
                for ($i = 0; $i < count($userIdArray); $i++) {
                    $assigneeDetails = array(
                        "task_id" => $request["task_id"],
                        "user_id" => $userIdArray[$i],
                        "created_by" => $request["created_by"]
                    );
                    $assignTask = new TaskAssignees;
                    $assignTask->fill($assigneeDetails);
                    if ($assignTask->save()) {
                        $status = true;
                    } else {
                        $status = false;
                    }

                    $userData = User::find($userIdArray[$i]);
                    array_push($newUsers, $userData->first_name . " " . $userData->last_name);
                }

                $taskData = Task::find($request["task_id"]);
                $eventData = array(
                    'type_id' => 'tasks',
                    'source_id' => $request["task_id"],
                    'data' => implode(",", $newUsers) . " added to " . $taskData->name . " by " . $user->first_name,
                );
                Event::fire(new TaskAssigneeAddEvent($eventData));

                return $status;
            } else {
                return true;
            }
        } catch (Exception $e) {
            $date = Carbon\Carbon::now();
            Log::error($date->toDateTimeString() . ' => Error occured while adding project assignee.');
            return $e->getMessage();
        }
    }

    //This Static Function is use in task controller for change the status of task
    //I have use two params in this function 
    //First params is task id and second is status value (note: it is not status Id it is value of status)
    public static function change_task_status($request) {
        self::update_task_order($request);

        //create a task model object for update a task
        $updateSingleTaskStatus = Task::find($request["task_id"]);
        //if status is present then save status else 0
        $updateSingleTaskStatus->status = isset($request["status"]) ? $request["status"] : 0;
        //check for change status update or not.
        if ($updateSingleTaskStatus->save()) {
            //if status save then return true else return false
            return true;
        } else {
            return false;
        }
    }

    public static function update_task_order($request) {
        $task = Task::find($request["task_id"]);
        $getAllTasks = Task::where("project_id", $task->project_id)->where("status", $request["status"])->get();
        if (count($getAllTasks) > 0 && !empty($getAllTasks) && isset($getAllTasks)) {
            foreach ($request['taskOrder'] as $data) {
                $updateOrder = Task::where('id', '=', $data['task_id'])->first();
                $updateOrder->order = $data['task_order'];
                $updateOrder->save();
            }
        } else if (count($getAllTasks) == 0) {
            $task->order = 1;
            $task->save();
        }
    }

    /*
     * Function for get task by task id
     * @params task Id
     */

    public static function get_task_assignee_by_task_id($task_id) {
        //Check task id present or not
        if (isset($task_id) && $task_id != '') {
            //Get task by task id
            $getTaskAssignees = TaskAssignees::with('user')->where('task_id', '=', $task_id)->get();
//            echo "<pre>";print_r($getTaskAssignees[0]->user);die;
            if (isset($getTaskAssignees) && !empty($getTaskAssignees) && count($getTaskAssignees) > 0) {
                return $getTaskAssignees;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }

    /*
     * Funcion for get all task assignee user
     * @params User ids.
     */

    public static function get_user_by_user_id_from_task_assignee($userIds) {
        //Explode the user ids into array

        $result = array();
        if (isset($userIds) && $userIds != '') {
            $ids = explode(",", $userIds);
            foreach ($ids as $key => $value) {
                $getUserByUserId = User::where('id', '=', $value)->first();
                $result[] = $getUserByUserId;
            }
            if (isset($result) && !empty($result) && count($result) > 0) {
                return $result;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }

    /*
     * Function for update Task Assignee
     * @params Task Id
     */

    public static function update_task_assignee($request, $taskId) {
        try {
            $user = JWTAuth::parseToken()->toUser();
            //check if task id present or not
            if (isset($taskId) && $taskId != '') {
                $taskAssignees = TaskAssignees::where("task_id", $taskId)->get();
                $currentAssignees = [];
                foreach ($taskAssignees as $taskAssignee) {
                    array_push($currentAssignees, $taskAssignee->user_id);
                }
                $newAssignees = [];
                $newAssignees = explode(",", $request->user_id);
                $common = array_intersect($currentAssignees, $newAssignees);
                $deleteAssignee = array_filter(array_diff($currentAssignees, $common));
                $addNewAssignee = array_filter(array_diff($newAssignees, $common));

                $taskAssigneeDetails = array(
                    "user_id" => implode(",", $addNewAssignee),
                    "task_id" => $taskId,
                    "created_by" => $user->id
                );

                if (!empty($addNewAssignee)) {
                    self::assign_task($taskAssigneeDetails);
                }

                if (!empty($deleteAssignee)) {
                    self::delete_task_assignee($deleteAssignee, $taskId);
                }
            }
        } catch (Exception $e) {
            return $e->message();
        }
    }

    public static function delete_task_assignee($assigneesID, $taskId) {
        try {
            $user = JWTAuth::parseToken()->toUser();
            $deleteUsers = array();
            foreach ($assigneesID as $assignee) {

                $userData = User::find($assignee);
                array_push($deleteUsers, $userData->first_name . " " . $userData->last_name);

                $deleteAssignee = TaskAssignees::where("user_id", $assignee)->where("task_id", $taskId)->first();
                $deleteAssignee->forceDelete();
            }

            $taskData = Task::find($taskId);
            $eventData = array(
                'type_id' => 'tasks',
                'source_id' => $taskId,
                'data' => implode(",", $deleteUsers) . " removed from " . $taskData->name . " by " . $user->first_name,
            );
            Event::fire(new TaskAssigneeRemoveEvent($eventData));
            return true;
        } catch (Exception $e) {
            $date = Carbon\Carbon::now();
            Log::error($date->toDateTimeString() . ' => Error occured while removing project assignee.');
            return $e->getMessage();
        }
    }

    public static function delete_assignee($request) {
        try {
            $status = true;
            $allTaskOfProject = Task::where("project_id", $request['projectId'])->get();
            if (isset($allTaskOfProject) && count($allTaskOfProject) > 0) {
                foreach ($allTaskOfProject as $task) {
                    if ($request["id"] == "") {
                        $getTaskAssignees = TaskAssignees::where("task_id", $task->id)->get();
                    } else {

                        $getTaskAssignees = TaskAssignees::where("task_id", $task->id)->where("user_id", $request['id'])->get();
                    }
                    if (isset($getTaskAssignees) && count($getTaskAssignees) > 0) {
                        foreach ($getTaskAssignees as $singleAssignee) {
                            if ($singleAssignee->forceDelete()) {
                                $status = true;
                            } else {
                                $status = false;
                            }
                        }
                    }
                }
            }
            return $status;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

}
