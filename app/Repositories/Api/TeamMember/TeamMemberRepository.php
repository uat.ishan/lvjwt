<?php

namespace App\Repositories\Api\TeamMember;

use App\Models\Access\TeamMember\TeamMember;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

/**
 * Class UserRepository.
 */
class TeamMemberRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = TeamMember::class;

    /**
     * @param RoleRepository $role
     */
    //function for get all teams members member
    public static function get_all_team_members() {
        $allTeamMembers = TeamMember::with('user','team')->get();
        if (isset($allTeamMembers) && !empty($allTeamMembers) && count($allTeamMembers) > 0) {
            return $allTeamMembers;
        } else {
            return array();
        }
    }

    //function for check teams member is present or not
    //if teams member is present then return teams member array
    //else return false
    //param $id is team member Id

    public static function check_team_member_present($id) {
        $checkTeamMemberPresent = TeamMember::find($id);
        if (isset($checkTeamMemberPresent) && !empty($checkTeamMemberPresent) && count($checkTeamMemberPresent) > 0) {
            return true;
        } else {
            return false;
        }
    }

    //function for update single team member
    //param $id is team member Id
    public static function update_single_team_member($request, $id) {
        //call function for check teams member is Present
        $checkTeamMemberPresent = self::check_team_member_present($id);
        if ($checkTeamMemberPresent) {
            //create object of teams member for update team member Detail
            $updateSingleTeamMember = TeamMember::find($id);
            $updateSingleTeamMember->fill($request->all());
            if ($updateSingleTeamMember->save()) {
                return $updateSingleTeamMember;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //function for create single team member
    //param $request is team member detail to be save
    public static function create_single_team_member($request) {
        //create object of team member
        $createSingleTeamMember = new TeamMember;
        $createSingleTeamMember->fill($request->all());
        $createSingleTeamMember->created_by = 1;
        if ($createSingleTeamMember->save()) {
            return $createSingleTeamMember;
        } else {
            return false;
        }
    }

}
