<?php

namespace App\Repositories\Api\Status;

use App\Models\Access\Status\Status;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use JWTAuth;
/**
 * Class UserRepository.
 */
class StatusRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Status::class;

    /**
     * @param RoleRepository $role
     */
    //function for get all status
    public static function get_all_status() {
        $allStatus = Status::all();
        if (isset($allStatus) && !empty($allStatus) && count($allStatus) > 0) {
            return $allStatus;
        } else {
            return array();
        }
    }


    //function for check status is present or not
    //if status is present then return status array
    //else return false
    //param $id is status Id

    public static function check_status_present($id) {
        $checkStatusPresent = Status::find($id);
        if (isset($checkStatusPresent) && !empty($checkStatusPresent) && count($checkStatusPresent) > 0) {
            return true;
        } else {
            return false;
        }
    }

    //function for update single status
    //param $id is status Id
    public static function update_single_status($request, $id) {
        //call function for check status is Present
        $checkStatusPresent = self::check_status_present($id);
        if ($checkStatusPresent) {
            //create object of status for update status Detail
            $updateSingleStatus = Status::find($id);
            $updateSingleStatus->fill($request->all());
            if ($updateSingleStatus->save()) {
                return $updateSingleStatus;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //function for create single status
    //param $request is status detail to be save
    public static function create_single_status($request) {
        //create object of status for update status Detail
        $createSingleStatus = new Status;
        $createSingleStatus->fill($request->all());
        if ($createSingleStatus->save()) {
            return $createSingleStatus;
        } else {
            return false;
        }
    }

    public static function predefined_status($projectId){
        /* parse token to get user details */
        $user = JWTAuth::parseToken()->toUser();

        /* A static array that contain four status that will be predined and created when new project is added */
        $statuses = array(
            '1' => array(
                'status_name' => 'To do', 
                'user_id' => $user->id, 
                'status_id' => 1,  
                'project_id' => $projectId  
            ),
            '2' => array(
                'status_name' => 'In progress', 
                'user_id' => $user->id, 
                'status_id' => 1,
                'project_id' => $projectId    
            ),
            '3' => array(
                'status_name' => 'Done', 
                'user_id' => $user->id, 
                'status_id' => 1,
                'project_id' => $projectId    
            ),
            '4' => array(
                'status_name' => 'Confirmed', 
                'user_id' => $user->id, 
                'status_id' => 1,
                'project_id' => $projectId    
            ),
        );
        try{
            foreach ($statuses as $statusKey => $statusValue){
                $statusObj = new Status;
                $statusObj->fill($statusValue);
                $statusObj->save();
            }
        }
        catch(Exception $e){
            return false;
        }
        return true;
        
    }

}
