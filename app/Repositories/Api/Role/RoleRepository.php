<?php
namespace App\Repositories\Api\Role;

use App\Models\Access\Role\Role;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

/**
 * Class UserRepository.
 */
class RoleRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Role::class;

    /**
     * @param RoleRepository $role
     */
    /* Function of repository will be called from RoleController index to get all records */
    public static function get_all_roles() {

        $allRoles = Role::all();
        if (isset($allRoles) && !empty($allRoles) && count($allRoles) > 0) {
            return $allRoles;
        } else {
            return array();
        }
    }

    /* Function to check if single role exist in database or not based on id */
    public static function check_role_present($id) {

        $checkRolePresent = Role::find($id);
        if (isset($checkRolePresent) && !empty($checkRolePresent) && count($checkRolePresent) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /* Fucntion to update single role will be called from RoleController */
    public static function update_single_role($request, $id) {

        /* Before update check if role is present or not */
        $checkRolePresent = self::check_role_present($id);
        if ($checkRolePresent) {
            $updateSingleRole = Role::find($id);
            $updateSingleRole->fill($request->all());
            if ($updateSingleRole->save()) {
                return $updateSingleRole;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    /* Static function to create a new role, will be called from RoleController */
    public static function create_single_role($request) {
        $createSingleRole = new Role;
        $createSingleRole->fill($request->all());
        if ($createSingleRole->save()) {
            return $createSingleRole;
        } else {
            return false;
        }
    }

}
