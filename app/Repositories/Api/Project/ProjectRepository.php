<?php

namespace App\Repositories\Api\Project;

use App\Models\Access\Project\Project;
use App\Models\Access\Task\Task;
use App\Models\Access\User\User;
use App\Models\Access\TaskAssignees\TaskAssignees;
use App\Models\Access\ProjectAssignees\ProjectAssignees;
use App\Events\Api\ProjectAssigneeAddEvent;
use App\Events\Api\ProjectAssigneeRemoveEvent;
use App\Events\Api\ProjectNameChangeEvent;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use JWTAuth;
use Event;

/**
 * Class UserRepository.
 */
class ProjectRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Project::class;

    /**
     * @param RoleRepository $role
     */
    //function for get all projects
    public static function get_all_projects() {
        $user = JWTAuth::parseToken()->toUser();

        $projects = [];
        $tasksAssigned = TaskAssignees::with('task.project')->where("user_id", "=", $user->id)->get();

        if (isset($tasksAssigned) && count($tasksAssigned) > 0) {
            foreach ($tasksAssigned->toArray() as $tasksAssign) {
                $data = Project::where("id", $tasksAssign['task']['project']['id'])->with("tasks.user")->with("statuses")->first();
                $projects[$data->id] = $data->toArray();
            }
        }

        $projectsAssigned = ProjectAssignees::with('project')->with('user')->where("user_id", "=", $user->id)->get();
        if (isset($projectsAssigned) && count($projectsAssigned) > 0) {
            foreach ($projectsAssigned->toArray() as $projectAssign) {
                $data = Project::where("id", $projectAssign['project']['id'])->with("tasks.user")->with("statuses")->first();
                $projects[$data->id] = $data->toArray();
            }
        }
        $userProjects = Project::where("created_by", "=", $user->id)->with("statuses")->with("tasks.user")->get();
        if (isset($userProjects) && count($userProjects) > 0) {
            foreach ($userProjects->toArray() as $userProject) {
                $projects[$userProject['id']] = $userProject;
            }
        }


        // echo "<pre>";print_r($projects);die;
        if (!empty($projects) && count($projects) > 0) {
            return $projects;
        } else {
            return array();
        }
    }

    //function for check project is present or not
    //if project is present then return project array
    //else return false
    //param $id is project Id

    public static function check_project_present($id) {
        $checkProjectPresent = Project::find($id);
        if (isset($checkProjectPresent) && !empty($checkProjectPresent) && count($checkProjectPresent) > 0) {
            return true;
        } else {
            return false;
        }
    }

    //function for update single project
    //param $id is project Id
    public static function update_single_project($request, $id) {
        try {

            $user = JWTAuth::parseToken()->toUser();

            //call function for check project is Present
            $checkProjectPresent = self::check_project_present($id);
            if ($checkProjectPresent) {
                //create object of project for update project Detail
                $updateSingleProject = Project::find($id);

                if ($updateSingleProject->name != $request->name) {
                    $projectEventData = array(
                        'type_id' => 'projects',
                        'source_id' => $id,
                        'data' => $user->first_name . " " . $user->last_name . " rename project " . $updateSingleProject->name . " to " . $request->name,
                    );
                    Event::fire(new ProjectNameChangeEvent($projectEventData));
                }

                $updateSingleProject->fill($request->all());
                if ($updateSingleProject->save()) {
                    $assignProjectDetails = array(
                        "project_id" => $updateSingleProject->id,
                        "user_id" => $request->user_id,
                        "created_by" => $user->id,
                    );
                    $currentProjectAssignee = ProjectAssignees::where("project_id", $updateSingleProject->id)->get();
                    if (count($currentProjectAssignee) == 0) {

                        $assignProjectStatus = self::assign_project($assignProjectDetails);
                        if ($assignProjectStatus) {
                            return $updateSingleProject;
                        }
                    } else {

                        $currentAssignees = [];
                        foreach ($currentProjectAssignee as $projectAssignee) {
                            array_push($currentAssignees, $projectAssignee->user_id);
                        }
                        $updateProjectAssignee = self::update_project_assignee($assignProjectDetails, $currentAssignees);
                        // print_r($updateSingleProject);die;
                        if ($updateProjectAssignee) {
                            return $updateSingleProject;
                        }
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            $date = Carbon\Carbon::now();
            Log::error($date->toDateTimeString() . ' => Error occured while renaming project.');
            return $e->getMessage();
        }
    }

    public static function assign_project($request) {
        try {
            $user = JWTAuth::parseToken()->toUser();
            /* Check if assignees array user_id is empty or not */
            if ($request["user_id"] != null || $request["user_id"] != "") {

                /* Explode array of user id */
                $userIdArray = explode(",", $request["user_id"]);
                $status = false;
                $newAssignee = array();
                for ($i = 0; $i < count($userIdArray); $i++) {
                    $assigneeDetails = array(
                        "project_id" => $request["project_id"],
                        "user_id" => $userIdArray[$i],
                        "created_by" => $user->id
                    );
                    $assignProject = new ProjectAssignees;
                    $assignProject->fill($assigneeDetails);
                    if ($assignProject->save()) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    $userData = User::find($userIdArray[$i]);
                    array_push($newAssignee, $userData->first_name . " " . $userData->last_name);
                }

                $projectData = Project::find($request["project_id"]);
                $eventData = array(
                    'type_id' => 'projects',
                    'source_id' => $request["project_id"],
                    'data' => implode(",", $newAssignee) . " assigned to project " . $projectData->name . " by " . $user->first_name,
                );
                Event::fire(new ProjectAssigneeAddEvent($eventData));

                return $status;
            } else {
                return true;
            }
        } catch (Exception $e) {
            $date = Carbon\Carbon::now();
            Log::error($date->toDateTimeString() . ' => Error occured while adding project assignee.');
            $e->message();
        }
    }

    public static function update_project_assignee($request, $currentAssignees) {
        $user = JWTAuth::parseToken()->toUser();
        /* Check if assignees array user_id is empty or not */
        if ($request["user_id"] != null || $request["user_id"] != "") {
            $newAssignees = [];
            $newAssignees = explode(",", $request["user_id"]);
            $common = array_intersect($currentAssignees, $newAssignees);
            $deleteAssignee = array_filter(array_diff($currentAssignees, $common));
            $addNewAssignee = array_filter(array_diff($newAssignees, $common));
            $projectAssigneeDetails = array(
                "user_id" => implode(",", $addNewAssignee),
                "project_id" => $request["project_id"],
                "created_by" => $user->id
            );

            // echo "current\n";print_r($currentAssignees);
            // echo "new\n";print_r($newAssignees);
            // echo "common\n";print_r($common);
            // echo "delte\n";print_r($deleteAssignee);
            // echo "add\n";print_r($addNewAssignee);die;


            if (!empty($addNewAssignee)) {
                $newAssignee = self::assign_project($projectAssigneeDetails);
            }

            if (!empty($deleteAssignee)) {
                $deleteAssignee = self::delete_project_assignee($deleteAssignee, $request["project_id"]);
            }
            return true;
        } else {
            return true;
        }
    }

    public static function delete_project_assignee($assigneesID, $projectId) {
        try {
            $user = JWTAuth::parseToken()->toUser();
            $deleteProjectAssignee = array();
            foreach ($assigneesID as $assignee) {
                $deleteAssignee = ProjectAssignees::where("user_id", $assignee)->where("project_id", $projectId)->first();
                $deleteAssignee->forceDelete();
                $userData = User::find($assignee);
                array_push($deleteProjectAssignee, $userData->first_name . " " . $userData->last_name);
            }

            $projectData = Project::find($projectId);
            $eventData = array(
                'type_id' => 'projects',
                'source_id' => $projectId,
                'data' => implode(",", $deleteProjectAssignee) . " removed from project " . $projectData->name . " by " . $user->first_name,
            );
            Event::fire(new ProjectAssigneeRemoveEvent($eventData));
            return true;
        } catch (Exception $e) {
            $date = Carbon\Carbon::now();
            Log::error($date->toDateTimeString() . ' => Error occured while removing project assignee.');
            return $e->getMessage();
        }
    }

    public static function get_project_assignee($projectId) {
        if ($projectId != null || $projectId != "") {
            $getProjectAssignee = ProjectAssignees::with('user')->where("project_id", $projectId)->get();
            if (isset($getProjectAssignee) && count($getProjectAssignee) > 0) {
                return $getProjectAssignee;
            } else {
                return array();
            }
        } else {
            return false;
        }
    }

    //function for create single project
    //param $request is project detail to be save
    public static function create_single_project($request) {
        $user = JWTAuth::parseToken()->toUser();

        //create object of project for update project Detail
        $createSingleProject = new Project;
        $createSingleProject->fill($request->all());
        $createSingleProject->created_by = $user->id;
        if ($createSingleProject->save()) {
            return $createSingleProject;
        } else {
            return false;
        }
    }

    //common function for get pagination of project
    public static function uims_pagination() {
        $project = Project::where('created_by', '=', 1)->skip(3)->take(8)->paginate(3);
        return $project;
    }

    //This Static Function is use in project controller for change the status of project
    //I have use two params in this function 
    //First params is project id and second is status value (note: it is not status Id it is value of status)
    public static function change_project_status($project_id, $status) {
        //create a project model object for update a project
        $updateSingleProjectStatus = Project::find($project_id);
        //if status is present then save status else 0
        $updateSingleProjectStatus->status = isset($status) ? $status : 0;
        //check for change status update or not.
        if ($updateSingleProjectStatus->save()) {
            //if status save then return true else return false
            return true;
        } else {
            return false;
        }
    }

    /* Function for create a random handle name for project
     * 
     * 
     */

    public static function create_random_handle($length = 5) {
        // please don't change this salt value ever
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function create_project_handle($projectName) {

        /* set received project name to lower case and insert "-" replacing white space */
        $projectName = strtolower($projectName);
        $projectName = str_replace(" ", "-", $projectName);

        /* set slug status to false untill unique handle not occurs */
        $slugStatus = false;
        $initialValue = 0;
        $projectHandle = $projectName;
        do {
            /* Check if handle exists with $initialValue( 1 ) */
            $projectNameExist = Project::where("handle", $projectHandle)->get();
            if (count($projectNameExist) > 0 && !empty($projectNameExist)) {
                $initialValue = $initialValue + 1;

                /* set handle name with incremented value and check again */
                $projectHandle = $projectName . "-" . $initialValue;
            } else {
                $slugStatus = true;
            }
        } while ($slugStatus == false);

        // print_r($projectHandle);die;
        return $projectHandle;
    }

    public static function get_task_assignee($projectId) {
        if ($projectId != null || $projectId != "") {
            $projectUsers = [];

            /* First get task of the projects from task table */
            $getProjectTasks = Task::where("project_id", $projectId)->get();
            if (isset($getProjectTasks) && count($getProjectTasks) > 0) {
                foreach ($getProjectTasks as $value) {

                    /* Loop on every task to get its assignee from taskassignee table  */
                    $getTaskAssignee = TaskAssignees::with('user')->where("task_id", $value->id)->distinct("user_id")->get();
                    if (count($getTaskAssignee) > 0) {
                        foreach ($getTaskAssignee as $task) {
                            array_push($projectUsers, $task['user']);
                        }
                    }
                }
                // print_r($projectUsers);die;
                if (count($projectUsers) > 0) {
                    return $projectUsers;
                } else {
                    return array();
                }
            } else {
                return array();
            }
        } else {
            return false;
        }
    }

    public static function delete_assignee($request) {
        try {
            $status = false;
            $projectAssignee = ProjectAssignees::where("project_id", $request['projectId'])->where("user_id", $request['id'])->first();
            if (isset($projectAssignee) && count($projectAssignee) > 0) {
                if ($projectAssignee->forceDelete()) {
                    $status = true;
                } else {
                    $status = false;
                }
            }
            return $status;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function get_availaible_users($projectTaskAssignees) {

        if (count($projectTaskAssignees) > 0) {
            $userIds = [];
            foreach ($projectTaskAssignees as $key => $user) {
                array_push($userIds, $user->id);
            }
            // echo $userIds; die;
            $allUsers = array();
            $users = User::whereNotIn('id', $userIds)->get();
            return $users;
        } else {
            return array();
        }
    }

}
