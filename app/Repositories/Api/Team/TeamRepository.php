<?php

namespace App\Repositories\Api\Team;

use App\Models\Access\Team\Team;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

/**
 * Class UserRepository.
 */
class TeamRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Team::class;

    /**
     * @param RoleRepository $role
     */
    //function for get all teams
    public static function get_all_teams() {
        $allTeams = Team::with('user')->get();
        if (isset($allTeams) && !empty($allTeams) && count($allTeams) > 0) {
            return $allTeams;
        } else {
            return array();
        }
    }

    //function for check team is present or not
    //if team is present then return team array
    //else return false
    //param $id is team Id

    public static function check_team_present($id) {
        $checkTeamPresent = Team::find($id);
        if (isset($checkTeamPresent) && !empty($checkTeamPresent) && count($checkTeamPresent) > 0) {
            return true;
        } else {
            return false;
        }
    }

    //function for update single team
    //param $id is team Id
    public static function update_single_team($request, $id) {
        //call function for check team is Present
        $checkTeamPresent = self::check_team_present($id);
        if ($checkTeamPresent) {
            //create object of team for update team Detail
            $updateSingleTeam = Team::find($id);
            $updateSingleTeam->fill($request->all());
            if ($updateSingleTeam->save()) {
                return $updateSingleTeam;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //function for create single team
    //param $request is team detail to be save
    public static function create_single_team($request) {
        //create object of team for update team Detail
        $createSingleTeam = new Team;
        $createSingleTeam->fill($request->all());
        $createSingleTeam->created_by = 1;
        if ($createSingleTeam->save()) {
            return $createSingleTeam;
        } else {
            return false;
        }
    }

    //common function for get pagination of team
    public static function uims_pagination() {
        $team = Team::where('created_by', '=', 1)->skip(3)->take(8)->paginate(3);
        return $team;
    }

}
