<?php

namespace App\Repositories\Api\Credentials;

use App\Models\Access\Credentials\Credentials;
use App\Models\Access\CommentType\CommentType;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use JWTAuth;
/**
 * Class UserRepository.
 */
class CredentialsRepository extends BaseRepository {
    
    /* Function to save credentials of project in database */
    public static function save_credential($request) {

        /* get user details from jwt token */
        $user = JWTAuth::parseToken()->toUser();
        $checkCredential = Credentials::where("project_id",$request->project_id)->first();
        
        /* If credentials of current project exist then update it else add new credentials in database */
        if (count($checkCredential) > 0 && !empty($checkCredential) && isset($checkCredential)) {

            /* Fill new data replacing existing record data in database */
            $checkCredential->data = $request->data;
            $checkCredential->fill($request->all());
            $checkCredential->data = encrypt($request->data);
            if($checkCredential->save()){
                
                $checkCredential = Credentials::where("id", $checkCredential->id)->first();
                $checkCredential->data = decrypt($checkCredential->data);
                return $checkCredential; 
            } else{
                return array();
            }
        } else {
            
            /* Create new object of Credentials model if record already doesnt exist */
            $credentialObj = new Credentials();
            $credentialObj->fill($request->all());
            $credentialObj->user_id = $user->id;
            $credentialObj->data = encrypt($request->data);

            if ($credentialObj->save()) {

                $credentials = Credentials::where("id", $credentialObj->id)->first();
                $credentials->data = decrypt($credentials->data);
                return $credentials;
            } else {
                return false;
            }
        }
    }
    
    public static function get_credential_by_projectId($projectId){
        
        /* Check if projectId is not null */
        if($projectId != "" && !empty($projectId)){
            $checkCredential = Credentials::where("project_id",$projectId)->first();
            if(isset($checkCredential) && count($checkCredential) > 0 && !empty($checkCredential)){
                $checkCredential->data  = decrypt($checkCredential->data);
                return $checkCredential;
            } else {
                return array();
            }
        }
    }
    
}
