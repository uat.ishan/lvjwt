<?php

namespace App\Repositories\Api\User;

use App\Models\Access\User\User;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
/**
 * Class UserRepository.
 */
class UserRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = User::class;

    /**
     * @var RoleRepository
     */
    protected $role;

    /**
     * @param RoleRepository $role
     */
    public static function get_users() {
        return User::all();
    }

    public static function emptyUser() {
        return new User();
    }

    public static function get_single_user($id) {
        $user = User::find($id);
        if (count($user)>0) {
            return $user;
        } else {
            return false;
        }
    }

    public static function update_user($request, $id) {
        //create object of project for update project Detail
        $updateSingleuser = User::find($id);
        $updateSingleuser->fill($request->all());
        if ($updateSingleuser->save()) {
            return $updateSingleuser;
        } else {
            return false;
        }
    }

    public static function check_password($currentPassword){
        $user = JWTAuth::parseToken()->toUser();
        if(Hash::check($currentPassword,$user->password)){
            return true;
        }
        else{
            return false;
        }
        
    }
    public static function change_password($request){
        $user = JWTAuth::parseToken()->toUser();
        $userRecord = User::find($user->id);
        $userRecord->password = bcrypt($request->password);
        if($userRecord->save()){
            return true;
        }
        else{
            return false;
        }
    }

}
