<?php

namespace App\Repositories\Api\Wiki;

use App\Models\Access\Wikis\Wikis;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use JWTAuth;

/**
 * Class UserRepository.
 */
class WikiRepository extends BaseRepository {

    public static function save_wiki($request) {

        /* get user details from jwt token */
        $user = JWTAuth::parseToken()->toUser();
        $checkWiki = Wikis::where("project_id",$request->project_id)->first();
        
        /* If wiki of current project exist then update it else add new wiki in database */
        if (count($checkWiki) > 0 && !empty($checkWiki) && isset($checkWiki)) {
            
            /*  */
            $checkWiki->data = $request->data;
            $checkWiki->fill($request->all());
            $checkWiki->data = encrypt($request->data);
            if($checkWiki->save()){
                
                $checkWiki = Wikis::where("id", $checkWiki->id)->first();
                $checkWiki->data = decrypt($checkWiki->data);
                return $checkWiki; 
            } else{
                return array();
            }
        } else {
            $wikiObj = new Wikis();
            $wikiObj->fill($request->all());
            $wikiObj->user_id = $user->id;
            $wikiObj->data = encrypt($request->data);

            if ($wikiObj->save()) {

                $wiki = Wikis::where("id", $wikiObj->id)->first();
                $wiki->data = decrypt($wiki->data);
                return $wiki;
            } else {
                return false;
            }
        }
    }
    
    public static function get_wiki_by_projectId($projectId){
        
        /* Check if projectId is not null */
        if($projectId != "" && !empty($projectId)){
            $checkWiki = Wikis::where("project_id",$projectId)->first();
            if(isset($checkWiki) && count($checkWiki) > 0 && !empty($checkWiki)){
                
                $checkWiki->data = decrypt($checkWiki->data);
                return $checkWiki;
            } else {
                return array();
            }
        }
    }

}
