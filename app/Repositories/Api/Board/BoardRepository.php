<?php

namespace App\Repositories\Api\Board;

use App\Models\Access\Status\Status;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

/**
 * Class UserRepository.
 */
class BoardRepository extends BaseRepository {

    const MODEL = Status::class;

    public static function delete_board($boardId) {
        $deleteStatus = Status::find($boardId);
        if ($deleteStatus->delete()) {
            if ($deleteStatus->delete()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
