<?php

namespace App\Listeners\Api;

use App\Events\Api\StatusChangeActivityEvent;
use App\Models\Access\Activities\Activities;
use App\Models\Access\User\User;
use App\Models\Access\ActivityType\ActivityType;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use JWTAuth;

/**
 * Class ActivityEventListener.
 */
class ActivityEventListener {

    protected $details;

    /**
     * @var string
     */
    public function handle($event) {
        $user = JWTAuth::parseToken()->toUser();
        $userData = User::find($user->id);
        $getActivityType = ActivityType::where("name", $event->data["type_id"])->first();
        if (count($getActivityType) > 0 && !empty($getActivityType) && isset($getActivityType)) {
            $activityObj = new Activities();
            $event->data["data"] = json_encode(array(
                'data' => $event->data["data"]
            ));

            $activityObj->fill($event->data);

            /* Update columns before saving in database */
            $activityObj->type_id = (int) $getActivityType->id;
            $activityObj->created_by = $user->id;

            if ($activityObj->save()) {
                // echo "done";
            } else {

                // echo "not done";
            }
        }
    }

    public function subscribe($events) {
        $events->listen(
                \App\Events\Api\StatusChangeActivityEvent::class, '\App\Listeners\Api\ActivityEventListener'
        );
        $events->listen(
                \App\Events\Api\TaskAssigneeAddEvent::class, '\App\Listeners\Api\ActivityEventListener'
        );
        $events->listen(
                \App\Events\Api\TaskAssigneeRemoveEvent::class, '\App\Listeners\Api\ActivityEventListener'
        );
        $events->listen(
                \App\Events\Api\ProjectAssigneeAddEvent::class, '\App\Listeners\Api\ActivityEventListener'
        );
        $events->listen(
                \App\Events\Api\ProjectAssigneeRemoveEvent::class, '\App\Listeners\Api\ActivityEventListener'
        );
        $events->listen(
                \App\Events\Api\ProjectNameChangeEvent::class, '\App\Listeners\Api\ActivityEventListener'
        );
    }

}
