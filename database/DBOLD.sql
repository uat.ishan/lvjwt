-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 19, 2017 at 06:17 PM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uims`
--

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `entity_id` int(10) UNSIGNED DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assets` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `history_types`
--

CREATE TABLE `history_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_12_28_171741_create_social_logins_table', 1),
(4, '2015_12_29_015055_setup_access_tables', 1),
(5, '2016_07_03_062439_create_history_tables', 1),
(6, '2017_04_04_131153_create_sessions_table', 1),
(7, '2017_03_16_070334_create_projects_table', 2),
(8, '2017_03_16_070344_create_tasks_table', 2),
(9, '2017_03_16_070355_create_status_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `handle`, `category`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'sdfsdf', 'sadasd', 'sdfds', 1, '2017-08-16 10:32:26', '2017-08-16 01:31:18', '2017-08-16 05:02:26'),
(2, 'uiaiaiaaafe', 'asdadasd', 'newproject', 1, '2017-08-16 10:14:18', '2017-08-16 01:34:46', '2017-08-16 04:44:18'),
(3, 'sdasdasdasd', 'pro', '34', 1, NULL, '2017-08-16 04:33:19', '2017-08-16 04:35:16'),
(4, 'xasdasd', 'sadas', 'asdsad', 1, NULL, '2017-08-16 04:55:12', '2017-08-16 04:55:12'),
(5, 'project', '123', 'ret', 1, NULL, '2017-08-17 06:13:41', '2017-08-17 06:13:41'),
(6, 'project', '123', 'ret', 1, NULL, '2017-08-17 23:09:03', '2017-08-17 23:09:03');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `all` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('ERAuJDGXCLpMwUVotxMNzbsiWcks0UbAkxE87EV8', NULL, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/60.0.3112.78 Chrome/60.0.3112.78 Safari/537.36', 'ZXlKcGRpSTZJa0UwWjNWQ2NXNVdiMjU2WjNOY0wzVkpkVzlvTXpsblBUMGlMQ0oyWVd4MVpTSTZJbVJIZG5ORmJuSnRiMkpsTTJ0UU9FUXdkekJ5VDNSQlRqTnpZbkpJVFdOTlZ6aERiMmh0ZFZKSmVrSXdaVmxzVVdneFJrOVBiVVppU1d0YVFqZE5VMUZ2VjBNNVdXRmhTRE5yV0cxek1URnpTbWx4VUU5UVNGVklTbmhXWkN0WFdsaEhTbGRTUWsxRlpHdENXVXB6UWpBMGVWTjFkVE5YZVRsRmREbHRSbU5IUkRSMmRsVkNiRkZxZUhkM2VqUnBTemMzUzNsSlV6bG1jakpITW5jMFkxVm9SbVpQUm05S01GQTBVSFZWUWpsMGJrRnJTekE1UWxkNmVEaEtWREZRTmxsREsxQjBjMnBCYzI5a1UxVlBiVzEwVFd4M1dWRXhORFZOUzJadVl6VmxiVVJZYVRGRWVFdDZVV0Z2YWpoS1FVOTZTV3hxUkdKNVlXdHNTMUZ5YkZWd1Z6bFJYQzl2ZEVkaVRWUkVTRWhJUld4SFZWbGNMMU0wV1ZrMWMwNTNaa3RRTW05cWQzUkZZMWxYUlU4eWNpdHRkbFpwVVZjNE1qTklZVGhjTDI1alRXSnFJaXdpYldGaklqb2lOelJqTWpnd05qRmxOemxrTkdObE5Ua3dNVE0wTXpRMVpETmxNamxrWkdObE1XTmtaVFZqWWpnNFpUWmpOekUyTldOaVltVmpaVGxrT0RjMk5tSTJPU0o5', 1502943285),
('JR72GZfhJDh3jIkjGWNodAb89UHErBMnHSJ6DXt0', NULL, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/60.0.3112.78 Chrome/60.0.3112.78 Safari/537.36', 'ZXlKcGRpSTZJbnBOUlVJM2VrdGhWRll6WTFoWmFWY3haMWhUTjJjOVBTSXNJblpoYkhWbElqb2lUV2t3TTBsTk9Gd3ZUMHhrYzJKb1UyMVZNalI0VGt4U1hDOTNjV3RESzA0NFNsTnJTMVU0Tm1WWU5VZElYQzlrZWpOdFpUazBOR3REWW1jNWExZFVZM0U0YUUxR1oyaHFTa2t3UWxkTGJFczNPQ3RPU0ZGRk1VSjFjVmhQVmxKcWNtdDBZMjlVWVc0NVZFZDBWMGhWVkdwWGJ6ZHVXR1Y0WmtWRVkxQTFOVWw0VTJZMVJIZFJaek54ZG05dk9FeFdkRzVNZEdoRGIwVjFTV0pQY1U1S1JEVmxVM1Z6WW5Nd1dVOXBXWEYyTVVOdk5GTmhiV3RwWjNwRk5uRm9VbkJTUmtaVFdrRmxVMlU1UTBwRWFEaHFPVmRTUXpCaFlWcGtOVE5GY1ZkYWEwTndhRUV4TUdkME5uaGpWR2x4VFZGUWNWVnlVVlpoWEM5blVIZGhWMU5MVkhCMmRsSlpNR3BFVm5scmVsRTVRVFpoVFVaY0wzQlhPRUZGZW5oMVNFcDNkRWwwYTJOa1RXeHpZMjVGVkc1c1lrTnRORFZoVlhSSE0yTkRWa3hzSzB4VFRtOXBRaUlzSW0xaFl5STZJakF5TjJOaU1UWXdNemcxTVRsaVlUWTBaREUwTkRrNU9HVTNNMlprWm1ObE5tUXlNR1JpWWpNNE4yVTJZMlV6TW1NMU56WTRPREkxWkdJeU5qZzBZekFpZlE9PQ==', 1502971149),
('KzX5EIMcbs8cJSz2QruY7LAPRlueFkJUy3Y1YKvS', NULL, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/60.0.3112.78 Chrome/60.0.3112.78 Safari/537.36', 'ZXlKcGRpSTZJbTVvYnpSVFdFZG9aR054V1dJeU1uSmxTSG8wUlZFOVBTSXNJblpoYkhWbElqb2liMlJRVGtwSlNGQTFZMUZvYzI1eGNHOTRNMHc0VmxwRVFpdDFOVE5tU0RCYVoyWnRkbUpuWEM5V2JFZGliME0yTmxaVGFVWm9NVGRDTlVJMFFVNU9ObEpGYlRBM1ZWWk1hV3cyYnl0S2VrTkJaRlo0WEM5VWFVVlllR3RJT1hwa1hDOVJZVVowVjJVM1RVcEdTa2xLVjI5bWMyNVZOVEV5VEd0WVFXcHhaalp1SzJWYVpWRmlibVpxWkc1amJtOHdORGR4V2tabk5sbDRVVzF4VkhwSGQxSjNiRk5DZGsxNU9IRkpabkYyVUdaaU5tcHpSa1pOTUROdVVUQTRUSGh5VURoeVFsd3ZOalJZYjNWU1Exd3ZPVzB6YW1KaE5XOXFRbE5OZG5NMVNsa3dOR3R6Ykc5R1kzSllNMWcyZEZaWVVuZHRVME5KYVVoQk1WQTBUV3RHVEZwVVpVbFZXVmhvYjI4NFpERnRPRUl3ZDJGVFdEQk9kRlpCVkVsVGQwWjVOaXMxZWpSU2FHd3dNbTVCVkVkWVNtZGFOWE5NYUUxWGJEUkhRek01ZGtsQ2MwVnNiU0lzSW0xaFl5STZJakJtWVRnd09USmxNbUV3TkRFeE5EYzJZalptTlRjMk56TXdaV1UzTXpZelptUTNaVE00Tnpsak5qZG1ORFkzWTJRd09XRTFaRFppTWpKbVpUWTJZbVFpZlE9PQ==', 1503128442);

-- --------------------------------------------------------

--
-- Table structure for table `social_logins`
--

CREATE TABLE `social_logins` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `status_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `status_name`, `user_id`, `status_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'open', 1, '1', '2017-08-17 10:46:29', '2017-08-17 05:08:02', '2017-08-17 05:16:29'),
(2, 'pending', 2, '3', '2017-08-17 10:56:10', '2017-08-17 05:16:05', '2017-08-17 05:26:10'),
(3, 'pending', 1, '1', NULL, '2017-08-17 05:24:43', '2017-08-17 07:04:13'),
(4, 'pending', 1, '1', NULL, '2017-08-17 05:28:05', '2017-08-17 06:51:14'),
(5, 'open', 1, '1', NULL, '2017-08-17 23:09:18', '2017-08-17 23:09:18');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `name`, `tag_name`, `project_id`, `status`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'sdsadasdas', 'hii', 1, '1', 0, NULL, NULL, '2017-08-16 05:41:18'),
(2, 'dffdsfsdfs', 'wewqerwqrwerwer', 1, '1', 1, '2017-08-16 10:16:44', NULL, '2017-08-16 04:46:44'),
(3, 'task3', 'tag', 2, '0', 1, '2017-08-16 10:35:13', '2017-08-16 02:13:55', '2017-08-16 05:05:13'),
(4, 'dummy', 'tag', 0, '0', 1, NULL, '2017-08-16 04:45:16', '2017-08-16 04:45:16'),
(5, 'sadadasd', 'asdsad', 0, '0', 1, NULL, '2017-08-16 06:51:10', '2017-08-16 06:51:10'),
(6, 'Task 1', 'p1t1', 0, '0', 1, NULL, '2017-08-17 06:12:24', '2017-08-17 06:12:24');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `lead_id`, `user_id`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'rahul', 1, 2, 1, '2017-08-19 11:43:33', '2017-08-19 11:43:33', '2017-08-19 06:13:33'),
(2, 'Team2', 2, 1, 1, NULL, '2017-08-19 11:35:22', '2017-08-19 06:05:22'),
(3, 'myw', 2, 1, 1, '2017-08-19 09:49:19', '2017-08-19 09:49:19', '2017-08-19 04:19:19'),
(4, 'Team1', 1, 1, 1, NULL, '2017-08-19 06:03:53', '2017-08-19 06:03:53'),
(5, 'Team1', 1, 1, 1, NULL, '2017-08-19 06:45:31', '2017-08-19 06:45:31');

-- --------------------------------------------------------

--
-- Table structure for table `team_members`
--

CREATE TABLE `team_members` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team_members`
--

INSERT INTO `team_members` (`id`, `user_id`, `team_id`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, NULL, '2017-08-19 05:31:30', '2017-08-19 05:31:30'),
(2, 3, 34, 1, '2017-08-19 11:04:09', '2017-08-19 11:04:09', '2017-08-19 05:34:09'),
(3, 4, 3, 1, NULL, '2017-08-19 11:50:07', '2017-08-19 06:20:07'),
(4, 2, 1, 1, '2017-08-19 11:53:16', '2017-08-19 11:53:16', '2017-08-19 06:23:16'),
(5, 2, 3, 1, NULL, '2017-08-19 06:53:44', '2017-08-19 06:53:44');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `status`, `confirmation_code`, `confirmed`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'ishan', 'negi', 'negiishan@gmail.com', '$2y$10$XSwDoW.lXuw.8GrQgCm4vuKkRVEhXbalcCMuZxYer2q6uGp7ZZZS6', 1, NULL, 1, 'ixqzS3fwBjVwIyoGvnsDFapwuYgdejA5DtxKzEIUY5QXAUubL2Qc3Gywn2z4', '2017-07-17 06:17:21', '2017-07-17 06:17:21', NULL),
(3, 'ishan', 'negi', 'negiishan1@gmail.com', '$2y$10$jb4aLJ0x0bf9LSlGEsWH.OO/qiC0I1ozyJXlWlcQLr3vo4pkUsf/G', 1, NULL, 1, NULL, '2017-07-18 02:57:26', '2017-07-18 02:57:26', NULL),
(4, 'ishan', 'negi', 'negiishan11@gmail.com', '$2y$10$.Vp84byyD0hG.WSY2vT84.Y8zDVv0GZKv8FQg0Fo.9dAOW3B9s2I2', 1, NULL, 1, NULL, '2017-07-19 07:32:51', '2017-07-19 07:32:51', NULL),
(5, 'ishan', 'negi', 'negiishan9999@gmail.com', '$2y$10$eyB0p/Rza6zua4mkeDe0MeqUei.FAk6Y0OwC1XHPmjCWYVJkB.OKW', 1, NULL, 1, NULL, '2017-07-19 08:14:28', '2017-07-19 08:14:28', NULL),
(6, 'ishan', 'negi', 'negiishan4@gmail.com', '$2y$10$7rgMBoLPsuJckQWGGWBifOer1IAsS.DyCWxW.CqIwmqnHEKH7novS', 1, NULL, 1, NULL, '2017-07-27 00:01:00', '2017-07-27 00:01:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_type_id_foreign` (`type_id`),
  ADD KEY `history_user_id_foreign` (`user_id`);

--
-- Indexes for table `history_types`
--
ALTER TABLE `history_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_logins_user_id_foreign` (`user_id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_members`
--
ALTER TABLE `team_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `history_types`
--
ALTER TABLE `history_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `social_logins`
--
ALTER TABLE `social_logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `team_members`
--
ALTER TABLE `team_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `history_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `history_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
