--------------------------------------------------------------------------------------------------------------------------------------------------
=============================================================  CONVENTIONS =======================================================================
--------------------------------------------------------------------------------------------------------------------------------------------------

This file contains details about the coding conventions that we are following in this project.
So It's a request you should follow all conventions to make code generic and easy to understand.

General note for this document which will make it easy to understand term used in this document:

    Camel case: For clarity, this document calls the two alternatives for term camel case.
        1. Upper camel case (initial upper case letter) example: CamelCase.
        2. Lower camel case (initial lower case letter) example: camelCase.




                                                                    General Notes
================================================================================================================================================
1. Use netbeans for coding to ensure same coding structure.
2. We are using four space tab.
3. There are different folders for api, frontend and backend. So please keep in mind you must write code in there respective locations.
4. Use traits if a single class contains lots of functions.



                                                                    Name conventions
================================================================================================================================================

1. Controller Name:
-----------------
Controller Name should be in Upper camel case and ends with(sufix) "Controller" . Example: TestController.php, TestDetailsController.php.

2. Class Name:
-----------------
Class Name should be in Upper camel case. Example: TestClassName{}, Test{}.
Also remember ClassName and FileName should be same.

2. Function Name:
-----------------
Function names must be separated with an underscore (_) and must contain all lower case letters. Example : test_function(), test().

3. Variable Name:
-----------------
Variable Name should be in lower camel case. Example: $testVariable, $test.

1. Folder Name:
----------------- 
Inside app folder: All Folder Name should be in Upper camel case. Example: TestFolder, Test.
Inside resources folder: All Folder Name should be in lower camel case and must be separated by -. Example: test-folder, test. Reason for this is URL structure this 
folder contains all the view files and results in URLS of pages.

File Name:
-----------------
Inside app folder: All file Name should be in Upper camel case. Example: TestFile.php, Test.php. Reason behind this is classes it's obvious we will create class 
inside almost each file in app folder so as mentioned above ClassName and FileName should be same.
Inside resources folder: All file names should be in lower camel case and must be separated by -. Example: test-folder.blade.php, test.blade.php. Reason for this is URL structure this 
folder contains all the view files and results in URLS of pages.


Namespaces:
-----------------
All namespaces should start from App directory and ends with the folder name in which that namespace/class resides.
Example: namespace App\Http\Controllers\Api\User
        For this nnamespace folder structure should be : App > Http > Controllers > Api > User > UserController.php and class name will be UserController.
        And to use it we simply write use App\Http\Controllers\Api\User\UserController;



                                                    Where to place your code or where you should write your code
========================================================================================================================================================================

Email/Notification related code:
---------------------------------
All the Email/Notification related code should be written inside app/notification folder.

Helpers:
----------
All the Helper function must be written inside Helper folder. It's strongly recommended that you must create a class for helper not a function.


